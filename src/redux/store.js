import { createStore } from 'redux'
import mapboxgl from 'mapbox-gl'
import maps from './reducers'
mapboxgl.accessToken = 'pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw';

const store = createStore(maps, { lng: 51.465831, lat: 35.690494, zoom: 12, profile: "driving",map : new mapboxgl.Map({
    container: "map",
    style: 'mapbox://styles/mapbox/streets-v12',
    center: [51.465831, 35.690494],
    zoom: 12
  }), selectingOrigin: false, destination: new mapboxgl.Marker({color: "red",draggable: true}), origin : new mapboxgl.Marker({color : "blue",draggable: true}) , originInputValue : "مکان فعلی" , destinationInputValue: "" , originSuggestions :[] , destinationSuggestions : [] , searchResults : [] , 
  infobox : {address : "" , title : "" , region: "" , type : "" , location :{}} , count : 0 , userView :"searchAndRouting" , vehicel : "car" , traffic : true , pollution : false ,switchBoutton : false , routes : []})
  
export default store