function maps(state = [], action) {
    switch (action.type) {
        case 'UPDATE_ORIGIN':
            return { ...state, origin: action.payload }
        case 'UPDATE_DESTINATION':
            return { ...state, destination: action.payload }
        case 'UPDATE_DESTINATION_SUGGESTIONS':
            return { ...state, destinationSuggestions: action.payload }
        case 'UPDATE_SEARCH_RESULTS':
            return { ...state, searchResults: action.payload }
        case 'UPDATE_ORIGIN_SUGGESTIONS':
            return { ...state, originSuggestions: action.payload }
        case 'UPDATE_ORIGIN_SELECTING':
            return { ...state, selectingOrigin: action.payload }
        case 'UPDATE_ORIGIN_INPUT_VALUE':
            return { ...state, originInputValue: action.payload }
        case 'UPDATE_DESTINATION_INPUT_VALUE':
            return { ...state, destinationInputValue: action.payload }
        case 'UPDATE_PROFILE':
            return { ...state, profile: action.payload }
        case 'UPDATE_COUNT':
            return { ...state, count: action.payload }
        case 'UPDATE_USER_VIEW':
            return { ...state, userView: action.payload }
        case 'UPDATE_INFOBOX':
            return { ...state, infobox: action.payload }
        case 'UPDATE_VEHICEL':
            return { ...state, vehicel: action.payload }
        case 'UPDATE_TRAFFIC':
            return { ...state, traffic: action.payload }
        case 'UPDATE_POLLUTION':
            return { ...state, pollution: action.payload }
        case 'UPDATE_SWITCH':
            return { ...state, switchBoutton: action.payload }
        case 'UPDATE_ROUTS':
            return { ...state, routes: action.payload }
        default:
            return state
    }
}

export default maps