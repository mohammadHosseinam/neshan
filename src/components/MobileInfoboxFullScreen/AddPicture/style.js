import styled from '@emotion/styled';

// Define the styled components
export const Container = styled.div`
  height: 212px;
  position: relative;
`;

export const Images = styled.div`
  display: flex;
  gap: 8px;
  overflow-x: scroll;
  scrollbar-width: none;
  -ms-overflow-style: none;
  ::-webkit-scrollbar {
    display: none;
  }
`;

export const Image = styled.div`
  width: 260px;
  height: 200px;
  background-color: rgba(129, 153, 203, 0.12);
  flex-shrink: 0;
`;

export const AddImageButton = styled.button`
  position: absolute;
  bottom: 0px;
  left: 16px;
  margin-top: -10px;
  border: 1.5px solid #1976D2;
  border-radius: 99px;
  padding: 12px 24px 12px 16px;
  display: flex;
  align-items: center;
  gap: 8px;
  font-family: Vazirmatn;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  color: #1976D2;
  background-color: #FFF;
`;