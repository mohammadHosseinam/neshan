import React from 'react';
import { Container, Images, Image, AddImageButton } from './style';
import cameraIcon from "../../../assets/cameraIcon.svg";

function AddPicture() {
  return (
    <Container>
      <Images>
        <Image />
        <Image />
        <Image />
      </Images>
      <AddImageButton>
        افزودن تصویر
        <img src={cameraIcon} alt="cameraIcon" />
      </AddImageButton>
    </Container>
  );
}

export default AddPicture;