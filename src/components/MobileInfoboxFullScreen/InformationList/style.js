import { css } from '@emotion/css';
export const myClasses = {
    countainer : css({ display: "flex", gap: "8px", padding: "8px" }),
    seperator : css({ width: "100%", height: "1.2px", backgroundColor: "#E7E7E7" }),
    isOpen : css({ fontWeight: "500", color: "#00CC9B", fontSize: "14px" }),
    phoneNumber: css({ fontWeight: "400", color: "#1976D2", fontSize: "14px" }),
    webAddress: css({ fontWeight: "400", color: "#1976D2", fontSize: "14px" })
  }