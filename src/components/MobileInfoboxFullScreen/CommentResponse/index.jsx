import React from 'react'
import { Countainer , RightSideLine , Flex , Date , CommentText , ProfileParent , Name } from './style'
import profile from "../../../assets/profile.svg"
function CommentResponse({text}) {
  return (
    <Countainer >
          <RightSideLine>
          </RightSideLine>
          <div>
          <Flex>
            <img src={profile} alt="profile" />
            <ProfileParent>
              <Name>مدیر کسب و کار</Name>
              <Date>۱۴۰۱/۰۷/۰۷</Date>
            </ProfileParent>
          </Flex>
          <CommentText >{text}
          </CommentText>
          </div>  
        </Countainer>
  )
}

export default CommentResponse