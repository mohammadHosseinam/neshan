import styled from '@emotion/styled';

export const Countainer = styled.div`
  margin: 13px 40px 0px 16px;
  padding-bottom: 13px;
  display: flex;
  gap: 8px;
`;

export const RightSideLine = styled.div`
  width: 4px;
  background-color: rgba(178, 178, 178, 0.20);
`;

export const Flex = styled.div`
  display: flex;
`;

export const Date = styled.p`
  font-size: 11px;
`;

export const CommentText = styled.p`
  font-size: 12px;
  font-family: Vazirmatn;
  color: #404040;
  margin-top: 7px;
  min-height: 96px;
  line-height: 24px;
  font-size: 14px;
  font-weight: 400;
`;

export const ProfileParent = styled.div`
  margin-right: 10px;
  flex-grow: 1;
`;

export const Name = styled.h5`
  font-size: 12px;
  font-weight: 500;
  font-family: Vazirmatn;
  color: #808080;
  margin-bottom: 8px;
`;
