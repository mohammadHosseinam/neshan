import React from 'react'
import { Container , Text , InputContainer , Input } from './style'
import FullInfoboxStarsRate from "../../../assets/FullInfoboxStarsRate.svg"
function Staring() {
    return (
      <Container>
        <Text>به این مکان امتیاز دهید</Text>
        <img src={FullInfoboxStarsRate} alt="FullInfoboxStarsRate" />
        <InputContainer>
          <Input type="text" placeholder="نظرتان را درباره این مکان بنویسید" />
        </InputContainer>
      </Container>
    );
  }
  
  export default Staring;