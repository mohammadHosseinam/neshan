import styled from '@emotion/styled';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  background-color: #F5F5F5;
`;

export const Text = styled.h4`
  color: #000;
  font-size: 16px;
  font-weight: 700;
  font-family: Vazirmatn;
  padding-top: 22px;
  margin-bottom: 14px;
  width: 184px;
  text-align: center;
`;

export const InputContainer = styled.div`
  padding: 16px 24px;
  width: 100%;
  display: flex;
`;

export const Input = styled.input`
  flex-grow: 1;
  background-color: #fff;
  border: 1px solid #D9D9D9;
  border-radius: 8px;
  height: 33px;
  padding: 12px 16px;
  color: #a6a6a6;
  font-size: 12px;
  font-family: Vazirmatn;
`;