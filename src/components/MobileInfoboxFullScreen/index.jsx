import React, { useState } from 'react'
import { CommentsTitle , Countainer } from './style.js'
import AddPicture from './addPicture/index.jsx'
import InfoboxIntro from './InfoboxIntro/index.jsx'
import InfoboxButtons from './infoboxButtons/index.jsx'
import SperatorLine from './SperatorLine/index.jsx'
import InformationList from './InformationList/index.jsx'
import Staring from './Staring/index.jsx'
import OptionsList from './OptionsList/index.jsx'
import Comment from './Comment/index.jsx'
import CommentResponse from './CommentResponse/index.jsx'
import { useDispatch } from 'react-redux'

function MobileInfoboxFullScreen() {
  const dispatch = useDispatch()
  const [isHidden, setIsHidden] = useState(false);
  let startY = 0;

  const handleTouchStart = (event) => {
    startY = event.touches[0].clientY;
  };

  const handleTouchMove = (event) => {
    const currentY = event.touches[0].clientY;
    const dragDistance = currentY - startY;
    const threshold = 100; // Adjust this value as needed

    if (dragDistance > threshold) {
      dispatch({type : "UPDATE_USER_VIEW" , payload : "infobox"})
      
    }
  };

  const handleTouchEnd = () => {
    startY = 0;
    
    console.log("iam working")

  };

  if (isHidden) {
    return null;
  }
  return (
    <Countainer onTouchEnd={handleTouchEnd} onTouchMove={handleTouchMove} onTouchStart={handleTouchStart}>
      <AddPicture />
      <InfoboxIntro />
      <InfoboxButtons />
      <SperatorLine />
      <InformationList />
      <Staring />
      <OptionsList />
      <div>
        <CommentsTitle>
          نظرات برگزیده
        </CommentsTitle>
        <SperatorLine />
        <Comment text="این رستوران اگرچه معروف است اما دو نکته مهم: متاسفانه به علت عدم وجود پارکینگ اختصاصی اکثر مشتریان با مشکل پارکینگ مواجه هستند. نکته دیگر اینکه با استفاده از مترو و وسایل عمومی می توان به رستوران دسترسی داشت." name="محمد مهدی مولایی" />
        <CommentResponse text={`با سلام و احترام
وقتتون بخیر
امیدوارم حال دلتون خوب باشه 
سپاس بی پایان از مهر ماندگار شما بزرگوار
با آرزوی بهترین سرویس بهداشتی‌ها`} />

        <SperatorLine />

        <Comment name="محمد مهدی مولایی" text="لورم ایپسوم متن ساختگی" />

      </div>
    </Countainer>
  )
}

export default MobileInfoboxFullScreen