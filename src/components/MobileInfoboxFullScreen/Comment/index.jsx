import React from 'react'
import { CommentContainer , CommentTextParagraph , CommentFlex , CommentProfileParent , CommentName , CommentFlexGrow , CommentLike} from './style'
import profile from "../../../assets/profile.svg"
import like from "../../../assets/like.svg"
import darkStar from "../../../assets/darkStar.svg"
import lightStar from "../../../assets/lightStar.svg"
import CommentMoreIcon from "../../../assets/CommentMoreIcon.svg"



function Comment({ text, name }) {
    return (
      <CommentContainer>
        <CommentFlex>
          <img src={profile} alt="profile" />
          <CommentProfileParent>
            <CommentName>{name}</CommentName>
            <CommentFlex>
              <img src={darkStar} alt="darkStar" />
              <img src={darkStar} alt="darkStar" />
              <img src={darkStar} alt="darkStar" />
              <img src={lightStar} alt="lightStar" />
              <img src={lightStar} alt="lightStar" />
              <CommentFlexGrow></CommentFlexGrow>
            </CommentFlex>
          </CommentProfileParent>
          <img src={CommentMoreIcon} alt="CommentMoreIcon" />
        </CommentFlex>
        <CommentTextParagraph>{text}</CommentTextParagraph>
        <CommentLike>
          <CommentFlexGrow></CommentFlexGrow>
          <p>۱۲</p>
          <img src={like} alt="like icon" />
        </CommentLike>
      </CommentContainer>
    );
  }
  
  export default Comment;