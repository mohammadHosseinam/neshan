import styled from '@emotion/styled';

export const CommentContainer = styled.div`
  margin: 13px 24px 0px;
  padding-bottom: 13px;
  border-bottom: 1px solid color: #f2f2f2;
`;

export const CommentFlex = styled.div`
  display: flex;
`;

export const CommentFlexGrow = styled.div`
  flex-grow: 1;
`;

export const CommentLike = styled.div`
  display: flex;
  gap: 8px;
`;

export const CommentTextParagraph = styled.p`
  font-size: 12px;
  font-family: Vazirmatn;
  color: #404040;
  margin-top: 7px;
  min-height: 96px;
  line-height: 24px;
  font-size: 14px;
  font-weight: 400;
`;

export const CommentProfileParent = styled.div`
  margin-right: 10px;
  flex-grow: 1;
`;

export const CommentName = styled.h5`
  font-size: 12px;
  font-weight: 500;
  font-family: Vazirmatn;
  color: #808080;
  margin-bottom: 8px;
`;