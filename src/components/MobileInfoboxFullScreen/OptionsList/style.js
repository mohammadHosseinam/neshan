import { css } from '@emotion/css';
export const myClasses = {
    option: css({ display: "flex", gap: "8px", padding: "14px 8px", fontSize: "14px" }),
    optionBlue: css({ display: "flex", gap: "8px", padding: "14px 8px", color: "#1976D2", fontSize: "14px" }),
    seprateLine: css({ width: "100%", height: "1.2px", backgroundColor: "#E7E7E7" }),

  }