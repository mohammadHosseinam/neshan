import styled from '@emotion/styled';

export const CommentsTitle = styled.div`color: #000 ; text-align: right ; font-family: Vazirmatn ; font-size: 16px ; font-style: normal ; font-weight: 500 ; line-height: 24px ; padding: 16px 24px 8px 24px;`
export const Countainer = styled.div`position: absolute ; bottom: 0 ; z-index: 3 ; width: 100% ; background-color: #fff ; height: 100vh; overflow-y : scroll ; scrollbar-width: none;
-ms-overflow-style: none;
::-webkit-scrollbar {
  display: none;
}`
