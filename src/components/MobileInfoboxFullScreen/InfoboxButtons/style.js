import styled from '@emotion/styled';

export const InfoboxButtonsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px 32px;
  max-width: 450px;
  margin: 0 auto;
`;

export const InfoboxButtonsText = styled.div`
  font-weight: 700;
  font-size: 12px;
  font-family: Vazirmatn;
  color: #1976D2;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const RoutingButton = styled.button`
border: none;
border-radius: 14px;
padding: 9.13px;
margin-bottom: 7.5px;
background: #1976D2;
display: flex;
align-items: center;
justify-content: center;
width: 52px;
height: 52px;
`;

export const AddButton = styled.button`
border: none;
border-radius: 14px;
padding: 9.13px;
margin-bottom: 8px;
background-color: rgba(0, 128, 255, 0.08);
display: flex;
align-items: center;
justify-content: center;
width: 52px;
height: 52px;
`;

export const WeightText = styled.p`
  font-weight: 500;
`;