import React from 'react'
import { InfoboxButtonsContainer , InfoboxButtonsText , RoutingButton , AddButton , WeightText } from './style'
import fullInfoboxRouting from "../../../assets/fullInfoboxRouting.svg"
import fullInfoboxSave from "../../../assets/fullInfoboxSave.svg"


function InfoboxButtons() {
  return (
    <InfoboxButtonsContainer>
      <InfoboxButtonsText>
        <div>
          <RoutingButton>
            <img src={fullInfoboxRouting} alt="fullInfoboxRouting" />
          </RoutingButton>
        </div>
        <p>مسیریابی</p>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={fullInfoboxSave} alt="fullInfoboxSave" />
          </AddButton>
        </div>
        <WeightText>تماس</WeightText>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={fullInfoboxSave} alt="fullInfoboxSave" />
          </AddButton>
        </div>
        <WeightText>ذخیره</WeightText>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={fullInfoboxSave} alt="fullInfoboxSave" />
          </AddButton>
        </div>
        <WeightText>ارسال</WeightText>
      </InfoboxButtonsText>
    </InfoboxButtonsContainer>
  );
}

export default InfoboxButtons;