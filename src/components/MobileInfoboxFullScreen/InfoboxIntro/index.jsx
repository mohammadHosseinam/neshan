import React from 'react'
import fullInfoboxStras from "../../../assets/fullInfoboxStras.svg"
import InfoboxPlaceIcon from "../../../assets/InfoboxPlaceIcon.svg"
import fullInfoboxLine from "../../../assets/fullInfoboxLine.svg"
import fullInfoboxCar from "../../../assets/fullInfoboxCar.svg"
import { InfoboxIntroContainer , InfoboxTitle , InfoboxPlaceType , InfoboxRate , InfoboxTimeToArive , MinutTextStyles , MinutIntegerStyles , Stars , Margin } from './style.js'

function InfoboxIntro() {
  return (
    <InfoboxIntroContainer>
      <InfoboxTitle> <img src={InfoboxPlaceIcon} alt="InfoboxPlaceIcon" /> رستوران باغ زیتون </InfoboxTitle>
      <InfoboxPlaceType>رستوران</InfoboxPlaceType>
      <InfoboxTimeToArive> <img src={fullInfoboxCar} alt="" />
        <MinutIntegerStyles>۱۲</MinutIntegerStyles> <MinutTextStyles>دقیقه</MinutTextStyles>
        <Margin src={fullInfoboxLine} alt="fullInfoboxLine"/>
        <MinutIntegerStyles>۴۵</MinutIntegerStyles>
        <MinutTextStyles>کیلومتر</MinutTextStyles>
        <InfoboxRate> ۳/۴ <Stars src={fullInfoboxStras} alt="fullInfoboxStras" /> ۸۳ رای </InfoboxRate>
      </InfoboxTimeToArive>
    </InfoboxIntroContainer>
  );
}

export default InfoboxIntro;