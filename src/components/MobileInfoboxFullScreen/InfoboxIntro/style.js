import styled from '@emotion/styled';

export const InfoboxIntroContainer = styled.div`background-color: #fff; padding: 16px 8px 8px 16px;`;

export const InfoboxTitle = styled.div`font-family: Vazirmatn; color: #000; font-size: 18px; font-weight: 700; display: flex; align-items: center; gap: 10px;`;

export const InfoboxPlaceType = styled.p`color: #808080; text-align: right; font-family: Vazirmatn; font-size: 14px; font-style: normal; font-weight: 400; line-height: normal; margin-right: 32px; margin-bottom: 6px;`;

export const InfoboxRate = styled.div`color: #808080; text-align: right; font-family: Vazirmatn; font-size: 11px; font-style: normal; font-weight: 400; line-height: normal; display: flex; align-items: center; margin-right: 26px;`;

export const InfoboxTimeToArive = styled.div` display: flex; align-items: center;`;

export const MinutTextStyles = styled.span`color: #404040; text-align: right; font-family: Vazirmatn; font-size: 14px; font-style: normal; font-weight: 500; line-height: normal`;

export const MinutIntegerStyles = styled.p`color: #000; font-family: Vazirmatn; font-size: 14px; font-style: normal; font-weight: 700; line-height: normal; margin-left: 4px ; margin-right : 4px`;

export const Stars = styled.img`margin-right: 5px`;

export const Margin = styled.img`margin: 0 12px`;