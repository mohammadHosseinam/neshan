import React from 'react'
import PlaceTypeButton from '../../MobileSearch/PlaceTypeButton'
import gasStationIcon from '../../../assets/gasStationIcon.svg'
import resturantIcon from '../../../assets/resturantIcon.svg'
import wcIcon from '../../../assets/wcIcon.svg'
import otherIcon from '../../../assets/otherIcon.svg'
import homeIcon from '../../../assets/homeIcon.svg'
import workIcon from '../../../assets/workIcon.svg'
import personalPlaces from '../../../assets/personalPlaces.svg'



import {OptionType , PlacesContainer , BluePlaces , BlackPlaces , SepratorLine , CategoriesOptions } from './style'


function SearchOptions() {
    return (
      <div>
        <CategoriesOptions>
          <OptionType>
            <img src={gasStationIcon} alt="gasStationIcon" />
            پمپ گاز
          </OptionType>
          <OptionType>
            <img src={gasStationIcon} alt="gasStationIcon" />
            پمپ بنزین
          </OptionType>
          <OptionType>
            <img src={wcIcon} alt="wcIcon" />
            سرویس بهداشتی
          </OptionType>
          <OptionType>
            <img src={resturantIcon} alt="resturantIcon" />
            رستوران
          </OptionType>
          <OptionType>
            <img src={otherIcon} alt="otherIcon" />
            سایر
          </OptionType>
        </CategoriesOptions>
        <PlacesContainer>
          <BluePlaces>
            <img src={homeIcon} alt="homeIcon" />
            <span>خانه</span>
          </BluePlaces>
          <SepratorLine></SepratorLine>
          <BluePlaces>
            <img src={workIcon} alt="workIcon" />
            <span>محل کار</span>
          </BluePlaces>
          <SepratorLine></SepratorLine>
          <BlackPlaces>
            <img src={personalPlaces} alt="personalPlaces" />
            <span>مکان‌های شخصی</span>
          </BlackPlaces>
        </PlacesContainer>
      </div>
    );
  }
  
  export default SearchOptions;