import styled from '@emotion/styled';

export const OptionType =styled.button`
padding-right: 5px; display: flex; flex-shrink: 0; flex-basis: max-content; gap: 7px ; background-color: #fff ; color: #000 ; font-family: "Vazirmatn"; align-items: center ;  margin-right: 12px ; border-radius: 50px ; padding: 4px 10px ; border: 1px solid rgba(0,0,0,.3) 
`
export const PlacesContainer = styled.div`
  display: flex;
  margin-bottom: 6px;
  background-color: #fff;
  padding: 16px 12px;
  font-size: 16px;
  font-weight: 600;
  font-family: 'Vazirmatn';
`;

export const BluePlaces = styled.div`

  display: flex;
  align-items: center;
  color: rgb(25, 118, 210);
`;

export const BlackPlaces = styled.div`
  display: flex;
  align-items: center;
`;

export const SepratorLine = styled.div`
  background: hsla(0, 0%, 50%, 0.341);
  height: 26px;
  margin: 0 10px;
  width: 1px;
`;

export const CategoriesOptions = styled.div`
  overflow-x: scroll;
  margin-bottom: 8px;
  display: flex;
  width: 100%;
  overflow-x: scroll;
  align-items: center;
  scrollbar-width: none;
  -ms-overflow-style: none;

  &::-webkit-scrollbar {
    display: none;
  }
`;