import React from 'react'
import { css } from '@emotion/css';
import { useDispatch, useSelector } from 'react-redux';
import { Countainer } from './style';
import SearchInput from './SearchInput';
import SearchOptions from './SearchOptions';
import SearchResults from './SearchResults';

function DesktopSearch() {
    const searchResults = useSelector(state => state.searchResults)
    return (
        <Countainer>
            <SearchInput/>
            {
                searchResults.length == 0 ?<SearchOptions/>: <SearchResults/>
            }
        </Countainer>
    )
}

export default DesktopSearch