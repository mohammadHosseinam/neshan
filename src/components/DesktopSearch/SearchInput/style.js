import styled from '@emotion/styled';

export const Container = styled.div`
  margin: 16px;
  padding: 8px;
  display: flex;
  align-items: center;
  background-color: #f5f5f5;
  border-radius: 14px;
  border: 1px solid #d9d9d9;
`;

export const RoutingButton = styled.button`
  border-radius: 8px;
  background: transparent;
  padding-top: 4px;
  padding-bottom: 0px;
  border: none;
`;

export const SearchInputTag = styled.input`
  background-color: #f5f5f5;
  border: none;
  flex-grow: 1;
  margin-right: 8px;
  font-family: Vazirmatn;
  font-size: 14px;
`;

export const SeparatorLine = styled.span`
  width: 1px;
  height: 24px;
  margin: 0 5px;
  background-color: #d9d9d9;
`;