import React from 'react'
import { useDispatch } from 'react-redux';
import { Container , SearchInputTag , SeparatorLine , RoutingButton  } from './style';
import menuIcon from "../../../assets/menuIcon.svg"
import closeIcon from "../../../assets/closeIcon.svg"
import searchIcon from "../../../assets/searchIcon.svg"



function SearchInput() {
    const dispatch = useDispatch();
  
    const searchFunction = (e, type) => {
      fetch(`https://api.neshan.org/v1/search?term=${e.target.value}&lat=36.313287&lng=59.578749`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then((resp) => resp.json())
        .then((data) => {
          console.log(data.items);
          dispatch({ type, payload: data.items });
        });
    };
  
    return (
      <Container>
        <img src={menuIcon} alt="menuIcon" />
        <SearchInputTag
          type="text"
          placeholder="جستجو"
          onChange={(e) => {
            searchFunction(e, "UPDATE_SEARCH_RESULTS");
          }}
        />
        <img src={searchIcon} alt="searchIcon" />
        <SeparatorLine />
        <RoutingButton
          onClick={() => {
            dispatch({ type: "UPDATE_USER_VIEW", payload: "searchAndRouting" });
            dispatch({type : "UPDATE_SEARCH_RESULTS" , payload : []});
          }}
        >
          <img src={closeIcon} alt="closeIcon" />
        </RoutingButton>
      </Container>
    );
  }
  
  export default SearchInput;