import styled from '@emotion/styled';

export const Container = styled.div`
  position: absolute;
  bottom: 0;
  z-index: 1;
  width: 100%;
`;

export const RoundButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  width: 44px;
  margin-right: 24px;
`;

export const RoutingButton = styled.button`
  padding: 10px;
  height: 44px;
  border-radius: 50%;
  background-color: #1976D2;
  border: none;
  box-shadow: 3.04494px 3.04494px 10.1498px rgba(0, 0, 0, .18);
`;

export const MyLocationButton = styled.button`
  padding: 10px;
  height: 44px;
  border-radius: 50%;
  background-color: #fff;
  border: none;
  box-shadow: 3.04494px 3.04494px 10.1498px rgba(0, 0, 0, .18);
`;

export const WhereToGo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #fff;
  border-radius: 50px;
  overflow: hidden;
  box-shadow: 0 2px 8px rgba(0, 0, 0, .18);
  margin: 16px;
  padding-left: 16px;
  color: #adadad;
`;

export const WhereToGoButton = styled.div`
  font-family: 'Vazirmatn';
  flex-grow: 1;
  height: 52px;
  padding: 12px 22px;
  border: none;
  align-content: center;
  background-color: transparent;
`;

export const FooterContainer = styled.div`
  display: flex;
  background-color: #fff;
  padding: 4px 0;
`;