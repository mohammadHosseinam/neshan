import React from 'react'
import { useDispatch } from 'react-redux'
import { Container , RoundButtonsContainer , RoutingButton , MyLocationButton , WhereToGo ,WhereToGoButton , FooterContainer } from './style.js'
import mapIcon from '../../assets/mapIcon.svg'
import kankashIcon from '../../assets/kankashIcon.svg'
import settingIcon from '../../assets/settingIcon.svg'
import mobileRoutingIcon from '../../assets/mobileRoutingIcon.svg'
import myLocationIcon from '../../assets/myLocationIcon.svg'
import searchIconMobile from '../../assets/searchIconMobile.svg'
import FooterButton from './footerButton/index.jsx'

function MobileSearchAndRouting() {
    const dispatch = useDispatch();
    return (
      <Container>
        <RoundButtonsContainer>
          <RoutingButton>
            <img src={mobileRoutingIcon} alt="mobileRoutingIcon" />
          </RoutingButton>
          <MyLocationButton>
            <img src={myLocationIcon} alt="myLocationIcon" />
          </MyLocationButton>
        </RoundButtonsContainer>
        <WhereToGo onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "search" }) }}>
          <WhereToGoButton>کجا می خوای بری؟</WhereToGoButton>
          <img src={searchIconMobile} alt="searchIconMobile" />
        </WhereToGo>
        <FooterContainer>
          <FooterButton src={mapIcon} color="#1976d2" text="نقشـه" />
          <FooterButton src={kankashIcon} color="#959595" text="کنکاش" />
          <FooterButton src={settingIcon} color="#959595" text="تنظیمات‌ من" />
        </FooterContainer>
      </Container>
    )
  }
  
  export default MobileSearchAndRouting;