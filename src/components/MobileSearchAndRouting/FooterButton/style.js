import styled from '@emotion/styled';

export const ButtonContainer = styled.div`
  flex-grow: 1;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const ButtonText = styled.p`
  color: ${props => props.color};
  font-size: 12px;
  font-weight: 500;
  font-family: 'Vazirmatn';
`;