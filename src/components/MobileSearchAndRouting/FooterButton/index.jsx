import React from 'react';
import { css } from '@emotion/react';
import {ButtonContainer , ButtonText} from "./style.js"

function FooterButton({ src, color, text }) {
  return (
    <ButtonContainer>
      <img src={src} alt="icon" />
      <ButtonText color={color}>{text}</ButtonText>
    </ButtonContainer>
  );
}

export default FooterButton;