import styled from '@emotion/styled';

export const DesktopLeftSideButtons = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  pointer-events: none;
  align-items: end;
  position: absolute;
  height: 100vh;
  left: 0px;
  z-index: 1;
`;

export const DownloadButton = styled.button`
  background-color: #1976D2;
  font-size: 12px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 4px 10px 4px 17px;
  font-family: Vazirmatn;
  border-radius: 20px;
  border: none;
`;

export const GroupButtons = styled.div`
  display: flex;
  align-items: center;
  border-radius: 16px;
  background: #FFF;
  box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.22);
  padding: 4px 12px;
`;

export const LocationButton = styled.button`
  border-radius: 20px;
  background: #FFF;
  box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.22);
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
`;

export const ZoomGroupButtons = styled.div`
  border-radius: 16px;
  background: #FFF;
  box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.22);
  display: flex;
  flex-direction: column;
  width: 32px;
  height: 64px;
  justify-content: center;
  align-items: center;
  margin-bottom: 12px;
  margin-top: 12px;
`;

export const LayerImage = styled.img`
  margin-bottom: 17px;
`;

export const ZoomItems = styled.img`

`;
export const ZoomButton = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  background-color: transparent;
  padding: 8px;
  border: none;
`
export const SeparatorLine = styled.div`
  width: 1px;
  height: 24px;
  margin: 0 5px;
  background-color: #d9d9d9;
`;

export const TopButtons = styled.div`
  display: flex;
  flex-direction: row-reverse;
  gap: 12px;
  align-items: center;
  justify-content: center;
  margin-top: 16px;
  margin-left: 16px;
  pointer-events: all;
`;

export const CenterButtons = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: end;
  width: 60px;
  margin-left: 16px;
  pointer-events: all;
`;