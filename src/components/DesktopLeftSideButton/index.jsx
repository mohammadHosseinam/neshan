import React from 'react'
import { CenterButtons, DesktopLeftSideButtons, DownloadButton, GroupButtons, LayerImage, LocationButton, SeparatorLine, TopButtons, ZoomButton, ZoomGroupButtons, ZoomItems } from './style.js'
import layerImage from "../../assets/layerImage.png"
import download from "../../assets/download.svg"
import logout from "../../assets/logout.svg"
import printIcon from "../../assets/printIcon.svg"
import setting from "../../assets/setting.svg"
import numpadIcon from "../../assets/numpadIcon.svg"
import increaseZoomIcon from "../../assets/increaseZoomIcon.svg"
import decreaseZoomIcon from "../../assets/decreaseZoomIcon.svg"
import zoomSepratorIcon from "../../assets/zoomSepratorIcon.svg"
import location from "../../assets/location.svg"
import { useSelector } from 'react-redux'

function DesktopLeftSideButton() {
    const map = useSelector(state => state.map)
    const userView = useSelector(state => state.userView)
    const increaseZoomFunction = () => {
        map.zoomIn()
    }
    const decreaseZoomFunction = () => {
        map.zoomOut()
    }
    return (
        <DesktopLeftSideButtons>
            <TopButtons>
                <DownloadButton>
                    <img src={download} alt="download" />
                    <span>دانلود نشان</span></DownloadButton>
                {
                    userView!=="routing"&&<GroupButtons>
                    <img src={setting} alt="setting" />
                    <SeparatorLine></SeparatorLine>
                    <img src={printIcon} alt="printIcon" />
                    <SeparatorLine></SeparatorLine>
                    <img src={logout} alt="logout" />
                </GroupButtons>
                }
                
            </TopButtons>
            <CenterButtons>
                <LocationButton>
                    <img src={numpadIcon} alt="numpadIcon" />
                </LocationButton>
                <ZoomGroupButtons>
                    <ZoomButton>
                        <img onClick={increaseZoomFunction} src={increaseZoomIcon} alt="increaseZoomIcon" />
                    </ZoomButton>
                    <img src={zoomSepratorIcon} alt="zoomSepratorIcon" />
                    <ZoomButton>
                        <img onClick={decreaseZoomFunction} src={decreaseZoomIcon} alt="decreaseZoomIcon" />
                    </ZoomButton>
                </ZoomGroupButtons>
                <LocationButton>
                    <img src={location} alt="location" />
                </LocationButton>
            </CenterButtons>
            <LayerImage src={layerImage} alt="layers images" />
        </DesktopLeftSideButtons>
    )
}

export default DesktopLeftSideButton
