import styled from "@emotion/styled";

export const ResultItem = styled.div`
  display: flex;
  align-items: center;
  gap: 4px;
  background-color: #fff;
  padding: 12px 16px;
  margin: 2.5px 16px;
  border-radius: 10px;
`;

export const BluePlaces = styled.div`
  display: flex;
  align-items: center;
  color: rgb(25, 118, 210);
`;

export const BlackPlaces = styled.div`
  display: flex;
  align-items: center;
`;

export const SeparatorLine = styled.div`
  background: hsla(0, 0%, 50%, 0.341);
  height: 26px;
  margin: 0 10px;
  width: 1px;
`;

export const Title = styled.h5`
  color: #000;
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
`;

export const Region = styled.span`
  color: gray;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 18px;
  margin-top: 3px;
`;

export const Address = styled.p`
  color: #404040;
  font-size: 12px;
  font-style: normal;
  font-weight: 500;
`;