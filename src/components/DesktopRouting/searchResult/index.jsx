import React from 'react'
import { useSelector } from 'react-redux';
import { Address, Region, ResultItem, Title } from "./style.js"
function Result({ title, region, address , handelClick }) {
    return (
        <ResultItem key={title} onClick={handelClick}>
            <img alt="" width={24} height={24} src="https://static.neshanmap.ir/poi/64/city.png" data-src="" draggable="false" />
            <div>
                <Title>{title}</Title>
                <Region>{region}</Region>
                <Address>{address}</Address>
            </div>
        </ResultItem>
    )
}

export default Result