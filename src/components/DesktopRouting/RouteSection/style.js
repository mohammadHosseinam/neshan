import styled from "@emotion/styled";

export const Route =styled.div`
   padding: 12px 16px;
   display: flex;
   flex-direction: column;
   gap: 8px;
   border-bottom: 1px solid #F5F5F5;
   width: 100%;
`

export const RouteName = styled.h3`
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 500;
`
export const RouteTime = styled.div`
  color: #000;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 13px;
  font-style: normal;
  font-weight: 400;
  display: flex;
  align-items: center;
`

export const SeparatorIconImg = styled.img`
  padding-right: 8px;
`;

export const IntigersBold = styled.span`
color: #000;
text-align: center;
font-family: "Vazirmatn";
font-size: 12px;
font-style: normal;
font-weight: 700;
padding:  0 8px;
`;

export const RouteButton = styled.span`
color: #000;
text-align: right;
font-family: "Vazirmatn";
font-size: 14px;
font-style: normal;
font-weight: 500;
display: flex;
align-items: center;
padding: 6px 10px;
gap: 4px;
border-radius: 50px;
border: 1px solid #E5E5E5;
background: #FFF;
flex-grow: 0;
width: 172px;
`;
