import React from 'react'
import { Route, RouteName, SeparatorIconImg, RouteTime, IntigersBold, RouteButton } from "./style.js"
import routeDetailIcon from "../../../assets/routeDetailIcon.svg"
import carIcon from "../../../assets/carIcon.svg"
import sepratorIcon from "../../../assets/sepratorIcon.svg"

function RouteSection({route}) {
    console.log(route)
    return (
        <Route>
            <RouteName>{route.summary}</RouteName>
            <RouteTime>
                <img src={carIcon} alt="carIcon" />
                <IntigersBold>{route.distance.value}</IntigersBold> دقیقه
                <SeparatorIconImg src={sepratorIcon} alt="separatorIcon" />
                <IntigersBold>{route.duration.value}</IntigersBold> کیلومتر
            </RouteTime>
            <RouteButton>

                <img src={routeDetailIcon} alt="routeDetailIcon" />
                <span>شیب و جزئیات مسیر</span>
            </RouteButton>
        </Route>
    )
}

export default RouteSection