import React from 'react'
import { RoutingApiOptions, RoutingApiOption, RoutingApiOptionText, RoutingApiOptionTitle, RoutingApiOptionDesc, RoutingApiOptionButtons, GoButton, DontGoButton, SwitchButton, } from "./style.js"
import dontGoRed from "../../../assets/dontGoRed.svg"
import dontGoGray from "../../../assets/dontGoGray.svg"
import GoGray from "../../../assets/GoGray.svg"
import GoBlue from "../../../assets/GoBlue.svg"
import switchBlueIcon from "../../../assets/switchBlueIcon.svg"
import switchBlackIcon from "../../../assets/switchBlackIcon.svg"

import { useDispatch, useSelector } from 'react-redux'

function ApiOptionsSection() {
    const dispatch = useDispatch()
    const pollution = useSelector(state => state.pollution)
    const traffic = useSelector(state => state.traffic)
    const switchBoutton = useSelector(state => state.switchBoutton)
    return (
        <RoutingApiOptions>
            <RoutingApiOption>
                <RoutingApiOptionText>
                    <RoutingApiOptionTitle>طرح ترافیک</RoutingApiOptionTitle>
                    <RoutingApiOptionDesc> از ساعت ۶ تا ۱۶</RoutingApiOptionDesc>
                </RoutingApiOptionText>
                <RoutingApiOptionButtons>
                    <GoButton onClick={()=>{dispatch({type: "UPDATE_TRAFFIC" , payload : false })}} color={!traffic ? "#1976D3" : "#808080"} border={!traffic ? "#1976D3" : "transparent"} background={!traffic ? "#E8F1FB" : "transparent"}>
                        <img src={!traffic ? GoBlue : GoGray} alt="GoGray" />
                        <span>آره برو</span>
                    </GoButton>
                    <DontGoButton onClick={()=>{dispatch({type: "UPDATE_TRAFFIC" , payload : true })}} color={traffic ? "#D64F55" : "#808080"} border={traffic ? "#1976D3" : "transparent"} background={traffic ? "#E8F1FB" : "transparent"}>
                        <img src={traffic ? dontGoRed : dontGoGray} alt="dontGoRed" />
                        <span>نه نرو</span>
                    </DontGoButton>
                </RoutingApiOptionButtons>
            </RoutingApiOption>
            <RoutingApiOption>
                <RoutingApiOptionText>
                    <RoutingApiOptionTitle>طرح آلودگی</RoutingApiOptionTitle>
                    <RoutingApiOptionDesc> از ساعت ۶ تا ۱۶</RoutingApiOptionDesc>
                </RoutingApiOptionText>
                <RoutingApiOptionButtons>
                    <GoButton  onClick={()=>{dispatch({type: "UPDATE_POLLUTION" , payload : false })}} color={!pollution ? "#1976D3" : "#808080"} border={!pollution ? "#1976D3" : "transparent"} background={!pollution ? "#E8F1FB" : "transparent"}>
                        <img src={!pollution ? GoBlue : GoGray} alt="GoGray" />
                        <span>آره برو</span>
                    </GoButton>
                    <DontGoButton  onClick={()=>{dispatch({type: "UPDATE_POLLUTION" , payload : true })}} color={pollution ? "#D64F55" : "#808080"} border={pollution ? "#1976D3" : "transparent"} background={pollution ? "#E8F1FB" : "transparent"}>
                        <img src={pollution ? dontGoRed : dontGoGray} alt="dontGoRed" />
                        <span>نه نرو</span>
                    </DontGoButton>
                </RoutingApiOptionButtons>
            </RoutingApiOption>
            <RoutingApiOption>
                <RoutingApiOptionText>
                    <RoutingApiOptionTitle>پرهیز از عوارضی</RoutingApiOptionTitle>
                    <RoutingApiOptionDesc>سعی می کنیم از عوارضی نریم</RoutingApiOptionDesc>
                    <RoutingApiOptionDesc> در مسیریابی آفلاین اعمال نمیشود</RoutingApiOptionDesc>
                </RoutingApiOptionText>
                <SwitchButton onClick={()=>{dispatch({type : "UPDATE_SWITCH" , payload : !switchBoutton})}}>
                    <img src={switchBoutton?switchBlueIcon:switchBlackIcon} alt="switchBlueIcon" />
                </SwitchButton>
            </RoutingApiOption>
        </RoutingApiOptions>
    )
}

export default ApiOptionsSection