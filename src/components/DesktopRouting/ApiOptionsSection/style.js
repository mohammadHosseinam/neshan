import styled from "@emotion/styled";

export const RoutingApiOptions = styled.div`
   display: flex;
   flex-direction: column;
   gap: 12px;
   padding: 0 16px 12px;
`

export const RoutingApiOption = styled.div`
   display: flex;
   align-items: center;
   justify-content: space-between;
`

export const RoutingApiOptionText = styled.div`
   
`

export const RoutingApiOptionTitle = styled.h4`
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 500;
   line-height: normal;
   margin-bottom: 4px;
`
export const RoutingApiOptionDesc = styled.p`
   color: #808080;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 11px;
   font-style: normal;
   font-weight: 500;
   line-height: normal;
   letter-spacing: -0.22px;
`
export const RoutingApiOptionButtons = styled.div`
   border-radius: 8px;
   border: 0.8px solid #F5F5F5;
   background: #FFF;
   padding: 3.2px;
   display: flex;
`
export const GoButton = styled.button(props => ({
   display: "flex",
   padding: "5.6px 17.6px",
   alignItems: "center",
   gap: "4.8px",
   borderRadius: "6.4px",
   background: `${props.background}`,
   border : `1.2px solid ${props.border}`,
   color: `${props.color}`,
   textAlign: "right",
   fontFamily: "Vazirmatn",
   fontSize: "12px",
   fontStyle: "normal",
   fontWeight: 500
}))

export const DontGoButton = styled.button(props => ({
   padding: "5.6px 17.6px",
   borderRadius: "6.4px",
   border: `1.2px solid ${props.border}`,
   background: `${props.background}`,
   display: "flex",
   alignItems: "center",
   gap: "4.8px",
   color: `${props.color}`,
   textAlign: "right",
   fontFamily: "Vazirmatn",
   fontSize: "12px",
   fontStyle: "normal",
   fontWeight: 500
}))

export const SwitchButton = styled.button`
   border : none;
   background-color: transparent;
   height: 27px;
   margin-top: 8px ;
`