import styled from "@emotion/styled";

export const UserPlaces = styled.div`
  display: flex;
  background-color: #fff; 
  padding: 12px 16px 0px;
  font-size: 16px;
  font-weight: 600;
  font-family: Vazirmatn;
  gap: 3px;
`;

export const SepreterLine = styled.div`
  background: hsla(0, 0%, 50%, 0.341);
  height: 26px;
  margin: 0 10px;
  width: 1px;
`;

export const UserPlace = styled.div(props => ({
   display: "flex",
   alignItems: "center",
   color: props.color,
}))