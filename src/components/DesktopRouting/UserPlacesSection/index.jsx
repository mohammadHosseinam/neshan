import React from 'react'
import homeIconLightBlue from "../../../assets/homeIconLightBlue.svg"
import workIcon from "../../../assets/workIcon.svg"
import personalPlaces from "../../../assets/personalPlaces.svg"
import { UserPlaces, UserPlace, SepreterLine } from "./style.js"

function UserPlacesSection() {
    return (
        <UserPlaces>
            <UserPlace color='#404040'>
                <img src={homeIconLightBlue} alt="icon here" />
                <span>خانه</span>
            </UserPlace>
            <SepreterLine />
            <UserPlace color='rgb(25, 118, 210)'>
                <img src={workIcon} alt="icon here" />
                <span>محل کار</span>
            </UserPlace>
            <SepreterLine />
            <UserPlace color='#404040'>
                <img src={personalPlaces} alt="icon here" />
                <span>مکان های شخصی</span>
            </UserPlace>
        </UserPlaces>
    )
}

export default UserPlacesSection