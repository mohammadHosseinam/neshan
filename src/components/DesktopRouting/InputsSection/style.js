import styled from "@emotion/styled";

export const InputsSectionDiv = styled.div`
   margin: 0px 16px 10px 16px;
   display: flex;
   align-items: center;
   gap: 8px;
`

export const OriginInputCountainer = styled.div`
   height: 38px;
   display: flex; 
   border : 1px solid #D9D9D9;
   border-radius: 10px;
   overflow: hidden;
   padding: 2px;
   margin-bottom: 14px;
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 400;
`
export const Input = styled.input`
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 400;
   padding-right: 14px;
   border: none;
   flex-grow: 1;
`

export const DestenationInputCountainer = styled.div`
height: 38px;
   display: flex; 
   border : 1px solid #D9D9D9;
   border-radius: 10px;
   overflow: hidden;
   padding: 2px;
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 400;

`
export const ChangeOriginButton = styled.button`
   color: #000;
   text-align: right;
   font-family: "Vazirmatn";
   font-size: 14px;
   font-style: normal;
   font-weight: 400;
   border : none;
   padding-left: 17.78px;
   background-color: transparent;
`
export const ChangeMarkersButton = styled.button`
   border : none;
   background-color: transparent;

`