import React from 'react'
import { ChangeMarkersButton, ChangeOriginButton, DestenationInputCountainer, Input, InputsSectionDiv, OriginInputCountainer } from './style'
import inputsRourinIcon from "../../../assets/inputsRourinIcon.svg"
import routingCloseIcon from "../../../assets/routingCloseIcon.svg"
import { useDispatch, useSelector } from 'react-redux'

function InputsSection() {
    const dispatch = useDispatch()
    const originInputValue = useSelector(state => state.originInputValue)
    const destinationInputValue = useSelector(state => state.destinationInputValue)
    
    const searchFunction = (e, type) => {
        fetch(`https://api.neshan.org/v1/search?term=${e.target.value}&lat=36.313287&lng=59.578749`, {
          method: "GET",
          headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
        })
          .then(resp => resp.json())
          .then(data => {
            console.log(data.items);
            dispatch({ type, payload: data.items });
            dispatch({type : "UPDATE_ROUTS" , payload : []})
          })}
    return (
        <InputsSectionDiv>
            <img src={inputsRourinIcon} alt="inputsRourinIcon" />
            <div style={{ flexGrow: 1 }}>
                <OriginInputCountainer>
                    <Input onChange={(e)=>{searchFunction(e , "UPDATE_ORIGIN_SUGGESTIONS");dispatch({ type: "UPDATE_ORIGIN_INPUT_VALUE", payload: e.target.value })}} onFocus={()=>{dispatch({type:"UPDATE_ORIGIN_SELECTING" , payload : true});dispatch({type : "UPDATE_DESTINATION_SUGGESTIONS" , payload : []})}} type="text" placeholder='مبدا' value={originInputValue} />
                    <ChangeOriginButton>تغییر</ChangeOriginButton>
                </OriginInputCountainer>
                <DestenationInputCountainer>
                    <Input type="text" placeholder='مقصد' value={destinationInputValue} onChange={(e)=>{searchFunction(e , "UPDATE_DESTINATION_SUGGESTIONS");dispatch({ type: "UPDATE_DESTINATION_INPUT_VALUE", payload: e.target.value })}} onFocus={()=>{dispatch({type:"UPDATE_ORIGIN_SELECTING" , payload : false});dispatch({type : "UPDATE_ORIGIN_SUGGESTIONS" , payload : []})}} onClick={(e)=> console.log(e.target.value)} />
                </DestenationInputCountainer>
            </div>
            <ChangeMarkersButton>
                <img src={routingCloseIcon} alt="routingCloseIcon" />
            </ChangeMarkersButton>
        </InputsSectionDiv>
    )
}

export default InputsSection