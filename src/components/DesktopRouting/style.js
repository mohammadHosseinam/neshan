import styled from "@emotion/styled";

export const Container = styled.div`
   background-color :#FFFFFF;
   width : 420px;
   position: absolute;
   z-index: 3;
   height: 100vh;
`

export const SepratorLineHorizental = styled.div`
  width : calc(100% - 32px);
  height: 1px;
  background-color: #F5F5F5;
  margin: 12px;
`;

export const RoutsCountainer  = styled.div`
   height: 240px;
   overflow: scroll;
   scrollbar-width: none;
   -ms-overflow-style: none;
   ::-webkit-scrollbar {
      display: none;
    }
`