import React, { useEffect } from 'react'
import { Container, SepratorLineHorizental, RoutsCountainer } from './style'
import VehicleTypesButtons from './VehicleTypes'
import InputsSection from './InputsSection'
import UserPlacesSection from './UserPlacesSection'
import ApiOptionsSection from './ApiOptionsSection'
import RouteSection from './RouteSection'
import { useDispatch, useSelector } from 'react-redux'
import Result from './searchResult'
import mapboxgl from 'mapbox-gl'

function DesktopRouting() {
    console.log("object")
    const dispatch = useDispatch()
    const destination = useSelector(state => state.destination)
    const origin = useSelector(state => state.origin)
    const map = useSelector(state => state.map)
    const routes = useSelector(state => state.routes)
    const originSuggestions = useSelector(state => state.originSuggestions)
    const destinationSuggestions = useSelector(state => state.destinationSuggestions)
    const vehicel = useSelector(state => state.vehicel)
    const traffic = useSelector(state => state.traffic)
    const HandelOriginSugesstion = (suggestion) => {
        origin.setLngLat([suggestion.location.x, suggestion.location.y]).addTo(map);
        map.flyTo({
            center: [suggestion.location.x, suggestion.location.y],
            essential: true
        });
        dispatch({ type: "UPDATE_ORIGIN_SUGGESTIONS", payload: [] });
        dispatch({ type: "UPDATE_ORIGIN_INPUT_VALUE", payload: suggestion.title });

        
    }
    const HandelDestinationSugesstion = (suggestion) => {
        console.log(suggestion)
        destination.setLngLat([suggestion.location.x, suggestion.location.y]).addTo(map);
        map.flyTo({
            center: [suggestion.location.x, suggestion.location.y],
            essential: true
        });
        dispatch({ type: "UPDATE_DESTINATION_SUGGESTIONS", payload: [] });
        dispatch({ type: "UPDATE_DESTINATION_INPUT_VALUE", payload: suggestion.title });
    }
    function decodePolyline(encoded) {
        var len = encoded.length;
        var index = 0;
        var array = [];
        var lat = 0;
        var lng = 0;
    
        while (index < len) {
            var b;
            var shift = 0;
            var result = 0;
            do {
                b = encoded.charAt(index++).charCodeAt(0) - 63; 
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
    
            var dlat = ((result & 1) !== 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++).charCodeAt(0) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            var dlng = ((result & 1) !== 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
    
            array.push( [lng * 1e-5, lat * 1e-5] );
        }
    
        return array;
    }
    useEffect(()=>{
        if (origin.getLngLat() && destination.getLngLat()) route()
    },[originSuggestions , destinationSuggestions , vehicel , traffic , ])
    const route = async () => {
        const originLocation = `${origin.getLngLat().lat},${origin.getLngLat().lng}`;
        const destinationLocation = `${destination.getLngLat().lat},${destination.getLngLat().lng}`;
        fetch(`https://api.neshan.org/v4/direction${traffic?"/no-traffic?":"?"}type=${vehicel==="motorcycle"?"motorcycle":"car"}&origin=${originLocation}&destination=${destinationLocation}&avoidTrafficZone=${traffic}`, {
            method: "GET",
            headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
        })
            .then(resp => resp.json())
            .then(data => {
                console.log(data.routes)
                dispatch({type : "UPDATE_ROUTS" , payload : [data?.routes[0].legs[0]]})
                const route = data.routes[0].overview_polyline.points;
                const coordinates = decodePolyline(route);
                const bounds = coordinates.reduce(
                    (bounds, coord) => bounds.extend(coord),
                    new mapboxgl.LngLatBounds(
                        coordinates[0],
                        coordinates[0]
                    )
                  );
                  map.fitBounds(bounds, {
                    padding: { top: 50, bottom: 50, left: 50, right: 470 },
                  });
                if (!map.getSource('route')) {
                    map.addSource('route', {
                        'type': 'geojson',
                        'data': {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'LineString',
                                'coordinates': coordinates
                            }
                        }
                    });
                } else {
                    console.log(coordinates)
                    map.getSource('route').setData({
                        'type': 'Feature',
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': coordinates
                        }
                    });
                }

                if (!map.getLayer('route')) {
                    map.addLayer({
                        id: 'route-border',
                        type: 'line',
                        source: 'route',
                        paint: {
                            'line-color': 'black',
                            'line-width': 8
                        }
                    })
                    map.addLayer({
                        'id': 'route',
                        'type': 'line',
                        'source': 'route',
                        'layout': {
                            'line-join': 'round',
                            'line-cap': 'round'
                        },
                        'paint': {
                            'line-color': '#861dfe',
                            'line-width': 6,
                        }
                    });
                }
            })
    }
    
    return (
        <Container>
            <VehicleTypesButtons />
            <InputsSection />
            <UserPlacesSection />
            <SepratorLineHorizental />
            <ApiOptionsSection />
            <div style={{ height: "8px", width: "100%", backgroundColor: "#F6F6F6" }}></div>

            <RoutsCountainer >
                {
                    routes.map(route => <RouteSection route={route} />)
                }
                {
                    originSuggestions.map(suggestion => <Result title={suggestion.title} region={suggestion.region} address={suggestion.address} handelClick={() => HandelOriginSugesstion(suggestion)} />)
                }
                {
                    destinationSuggestions.map(suggestion => <Result title={suggestion.title} region={suggestion.region} address={suggestion.address} handelClick={() => HandelDestinationSugesstion(suggestion)} />)
                }
            </RoutsCountainer>
        </Container>
    )
}

export default DesktopRouting