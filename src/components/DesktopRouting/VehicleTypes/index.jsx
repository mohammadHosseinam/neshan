import React from 'react'
import { CloseButton, CloseButtonCountainer, Vehicle ,VehicleTypes } from './style'
import routingBlackCarIcon from "../../../assets/routingBlackCarIcon.svg"
import routingBlueCarIcon from "../../../assets/routingBlueCarIcon.svg"
import busRoutinBlueIcon from "../../../assets/busRoutinBlueIcon.svg"
import busRoutinBlackIcon from "../../../assets/busRoutinBlackIcon.svg"
import walkingBlackIcon from "../../../assets/walkingBlackIcon.svg"
import walkingBlueIcon from "../../../assets/walkingBlueIcon.svg"
import bicycleBlackIcon from "../../../assets/bicycleBlackIcon.svg"
import bicycleBlueIcon from "../../../assets/bicycleBlueIcon.svg"
import motorCycleBlackIcon from "../../../assets/motorCycleBlackIcon.svg"
import motorCycleBlueIcon from "../../../assets/motorCycleBlueIcon.svg"
import closeButtonRoutingIcon from "../../../assets/closeButtonRoutingIcon.svg"
import { useDispatch, useSelector } from 'react-redux'

function VehicleTypesButtons() {
  const vehicel = useSelector(state => state.vehicel)
  const map = useSelector(state => state.map)
  const origin = useSelector(state => state.origin)
  const destination = useSelector(state => state.destination)

  
  const dispatch = useDispatch()
  return (
    <VehicleTypes>
                <Vehicle backgroundColor={vehicel==="car"?"#fffff":"#F5F5F5"} color={vehicel==="car"?"#1976D2":"#404040"} borderColor={vehicel==="car"?"#1976D2":"#F5F5F5"} weight={vehicel==="car"?500:400} onClick={()=>{dispatch({type : "UPDATE_VEHICEL" , payload : "car"})}}>
                    <img src={vehicel==="car"?routingBlueCarIcon:routingBlackCarIcon} alt="routingBlackCarIcon" />
                    <span>خودرو</span>
                </Vehicle>
                <Vehicle backgroundColor={vehicel==="bus"?"#fffff":"#F5F5F5"} color={vehicel==="bus"?"#1976D2":"#404040"} borderColor={vehicel==="bus"?"#1976D2":"#F5F5F5"} weight={vehicel==="bus"?500:400} onClick={()=>{dispatch({type : "UPDATE_VEHICEL" , payload : "bus"})}}>
                    <img src={vehicel==="bus"?busRoutinBlueIcon:busRoutinBlackIcon} alt="busRoutinBlueIcon" />
                    <span>همگانی</span>
                </Vehicle>
                <Vehicle backgroundColor={vehicel==="walk"?"#fffff":"#F5F5F5"} color={vehicel==="walk"?"#1976D2":"#404040"} borderColor={vehicel==="walk"?"#1976D2":"#F5F5F5"} weight={vehicel==="walk"?500:400} onClick={()=>{dispatch({type : "UPDATE_VEHICEL" , payload : "walk"})}}>
                    <img src={vehicel==="walk"?walkingBlueIcon:walkingBlackIcon} alt="walkingBlackIcon" />
                    <span>پیاده</span>
                </Vehicle>
                <Vehicle backgroundColor={vehicel==="bicycle"?"#fffff":"#F5F5F5"} color={vehicel==="bicycle"?"#1976D2":"#404040"} borderColor={vehicel==="bicycle"?"#1976D2":"#F5F5F5"} weight={vehicel==="bicycle"?500:400} onClick={()=>{dispatch({type : "UPDATE_VEHICEL" , payload : "bicycle"})}}>
                    <img src={vehicel==="bicycle"?bicycleBlueIcon:bicycleBlackIcon} alt="bicycleBlackIcon" />
                    <span>دوچرخه</span>
                </Vehicle>
                <Vehicle backgroundColor={vehicel==="motorcycle"?"#fffff":"#F5F5F5"} color={vehicel==="motorcycle"?"#1976D2":"#404040"} borderColor={vehicel==="motorcycle"?"#1976D2":"#F5F5F5"} weight={vehicel==="motorcycle"?500:400} onClick={()=>{dispatch({type : "UPDATE_VEHICEL" , payload : "motorcycle"})}}>
                    <img src={vehicel==="motorcycle"?motorCycleBlueIcon:motorCycleBlackIcon} alt="motorCycleBlackIcon" />
                    <span>موتور</span>
                </Vehicle>
                <CloseButtonCountainer>
                    <CloseButton onClick={()=>{dispatch({type : "UPDATE_USER_VIEW" , payload : "searchAndRouting"});map.getSource('route').setData({
                        'type': 'Feature',
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': []
                        }
                    });origin.remove();destination.remove();origin._lngLat = { lng: "", lat: "" };destination._lngLat = { lng: "", lat: "" };dispatch({type:"UPDATE_DESTINATION_INPUT_VALUE" , payload :""});dispatch({type:"UPDATE_ORIGIN_INPUT_VALUE" , payload :""})}}>
                        <img src={closeButtonRoutingIcon} alt="closeButtonRoutingIcon" />
                    </CloseButton>
                </CloseButtonCountainer>
            </VehicleTypes>
  )
}

export default VehicleTypesButtons