import styled from "@emotion/styled";

export const VehicleTypes = styled.div`
   margin : 32px 16px 16px 16px;
   padding-right: 22px;
   display: flex;
   align-items: "center";
   gap: 8px;
`
export const Vehicle = styled.button(props => ({
   display: "flex",
   flexGrow: 0,
   padding: "8px",
   flexDirection: "column",
   justifyContent: 'center',
   alignItems: "center",
   gap: "2px",
   width: "60.4px",
   height: "58px",
   borderRadius: "5.846px",
   background: `${props.backgroundColor}`,
   border : `2px solid ${props.borderColor}`,
   color: `${props.color}`,
   textAlign: "center",
   fontFamily: "Vazirmatn",
   fontSize: "12px",
   fontStyle: "normal",
   fontWeight: props.weight
}))

export const CloseButton = styled.button`
   flex-grow: 0;
   width: 24px;
   height: 24px;
   background-color: transparent;
   border: none;
`
export const CloseButtonCountainer = styled.div`
  display : flex ; 
  align-items : center;
`
