import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container , Title , Region , Address } from "./style.js"

function SearchResult({ item }) {
  const map = useSelector(state => state.map);
  const destination = useSelector(state => state.destination);
  const dispatch = useDispatch();

  return (
    <Container
      onClick={() => {
        dispatch({ type: 'UPDATE_INFOBOX', payload: { address: item.address, title: item.title, region: item.region, type: item.type, location: item.location } });
        dispatch({ type: 'UPDATE_USER_VIEW', payload: 'infobox' });
        dispatch({type : "UPDATE_SEARCH_RESULTS" , payload : []});
        destination.setLngLat([item.location.x, item.location.y]).addTo(map);
        map.flyTo({
          center: [item.location.x, item.location.y],
          essential: true,
        });
      }}
    >
      <img alt="" width={24} height={24} src="https://static.neshanmap.ir/poi/64/city.png" data-src="" draggable="false" />
      <div>
        <Title>{item.title}</Title>
        <Region>{item.region}</Region>
        <Address>{item.address}</Address>
      </div>
    </Container>
  );
}

export default SearchResult;