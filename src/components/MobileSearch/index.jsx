import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Container , SearchBox , BackButton , SearchInput , PlaceTypes , UserPlaces , SepreterLine , ResultsContainer } from './style.js'
import PlaceTypeButton from './PlaceTypeButton/index.jsx'
import UserPlace from './userPlace/index.jsx'
import SearchResult from './SearchResult/index.jsx'
import gasStationIcon from '../../assets/gasStationIcon.svg'
import resturantIcon from '../../assets/resturantIcon.svg'
import wcIcon from '../../assets/wcIcon.svg'
import otherIcon from '../../assets/otherIcon.svg'
import homeIcon from '../../assets/homeIcon.svg'
import workIcon from '../../assets/workIcon.svg'
import personalPlaces from '../../assets/personalPlaces.svg'
import backIcon from '../../assets/backIcon.svg'

function MobileSearch() {
    const searchResults = useSelector(state => state.searchResults);
    const dispatch = useDispatch();
  
    const searchFunction = (e, type) => {
      fetch(`https://api.neshan.org/v1/search?term=${e.target.value}&lat=36.313287&lng=59.578749`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then(resp => resp.json())
        .then(data => {
          console.log(data.items);
          dispatch({ type, payload: data.items });
        });
    };
  
    return (
      <Container>
        <SearchBox>
          <BackButton onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "searchAndRouting" });dispatch({type : "UPDATE_SEARCH_RESULTS" , payload : []}) }}>
            <img src={backIcon} alt="backIcon" />
          </BackButton>
          <SearchInput type="text" placeholder="جستجو" onChange={e => { searchFunction(e, "UPDATE_SEARCH_RESULTS") }} />
        </SearchBox>
        {
            searchResults.length == 0?<div>
            <PlaceTypes>
              <PlaceTypeButton src={gasStationIcon} text="پمپ گاز" />
              <PlaceTypeButton src={gasStationIcon} text="پمپ بنزین" />
              <PlaceTypeButton src={wcIcon} text="سرویس بهداشتی" />
              <PlaceTypeButton src={resturantIcon} text="رستوران" />
              <PlaceTypeButton src={otherIcon} text="سایر" />
            </PlaceTypes>
            <UserPlaces>
              <UserPlace src={homeIcon} text="خانه" />
              <SepreterLine />
              <UserPlace src={workIcon} text="محل کار" />
              <SepreterLine />
              <UserPlace src={personalPlaces} text="مکان های شخصی" />
            </UserPlaces>
            </div>:<ResultsContainer>
          {searchResults.map(item => <SearchResult item={item} />)}
        </ResultsContainer>
        }
      </Container>
    );
  }
  
  export default MobileSearch;