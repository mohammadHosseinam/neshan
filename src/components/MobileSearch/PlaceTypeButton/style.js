import styled from '@emotion/styled';

export const Container = styled.div`
  display: flex;
  height: 35px;
  gap: 7px;
  flex-shrink: 0;
  background-color: #fff;
  color: #000;
  font-family: Vazirmatn;
  align-items: center;
  margin-right: 12px;
  border-radius: 50px;
  padding: 4px 10px;
`;
