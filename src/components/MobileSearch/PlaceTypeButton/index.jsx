import React from 'react';
import { css } from '@emotion/react';
import { Container } from "./style.js"

function PlaceTypeButton({ src, text }) {
  return (
    <Container>
      <img src={src} alt="icon here" />
      {text}
    </Container>
  );
}

export default PlaceTypeButton;