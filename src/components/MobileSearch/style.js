import styled from '@emotion/styled';

export const Container = styled.div`
  z-index: 2;
  width: 100%;
  height: 100vh;
  background-color: #f5f5f5;
  position: absolute;
  top: 0;
`;

export const SearchBox = styled.div`
  margin: 12px;
  padding: 0 16px;
  background-color: #fff;
  border-radius: 50px;
  height: 53px;
  display: flex;
  align-items: center;
`;

export const BackButton = styled.button`
  background-color: transparent;
  border: none;
  padding: 0px;
  margin: 0px;
`;

export const SearchInput = styled.input`
  flex-grow: 1;
  border: none;
  padding: 14px;
  font-family: Vazirmatn;
  font-size: 14px;
`;

export const PlaceTypes = styled.div`
  margin-bottom: 8px;
  display: flex;
  width: 100%;
  overflow-x: scroll;
  align-items: center;
  scrollbar-width: none;
    -ms-overflow-style: none;
    ::-webkit-scrollbar {
      display: none;
    }
`;

export const UserPlaces = styled.div`
  display: flex;
  margin-bottom: 6px;
  background-color: #fff; 
  padding: 16px 12px;
  font-size: 16px;
  font-weight: 600;
  font-family: Vazirmatn;
  gap: 3px;
`;

export const SepreterLine = styled.div`
  background: hsla(0, 0%, 50%, 0.341);
  height: 26px;
  margin: 0 10px;
  width: 1px;
`;

export const ResultsContainer = styled.div`
  height: calc(100vh - 53px);
  overflow: scroll;
  scrollbar-width: none;
    -ms-overflow-style: none;
    ::-webkit-scrollbar {
      display: none;
    }
`;
