import styled from '@emotion/styled';

export const UserPlaceStyles = styled.div`
  display: flex;
  align-items: center;
  color: rgb(25, 118, 210);
`;