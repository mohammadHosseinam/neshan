import React from 'react';
import { UserPlaceStyles } from "./style.js"

function UserPlace({ src, text }) {
  return (
    <UserPlaceStyles>
      <img src={src} alt="icon here" />
      {text}
    </UserPlaceStyles>
  );
}

export default UserPlace;