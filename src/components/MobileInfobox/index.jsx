import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Container, CloseButton, PlaceImage, InfoContainer, NameParent, PlaceName, ShowMoreButton, TypeSection, TypeText, Time, IsOpen, Rating, ButtonsContainer, RoutingButton, LightButton, Location, Drive, ImageWhitMargin, SeparatorIconImg } from './style'
import infoboxCloseButton from '../../assets/infoboxCloseButton.svg'
import placeTypeIcon from '../../assets/placeTypeIcon.svg'
import ShowMoreIcon from '../../assets/ShowMoreIcon.svg'
import wheelChair from '../../assets/wheelChair.svg'
import mobileInfoboxStar from '../../assets/mobileInfoboxStar.svg'
import carIcon from '../../assets/carIcon.svg'
import sepratorIcon from '../../assets/sepratorIcon.svg'
import locationIcon from '../../assets/locationIcon.svg'
import infoboxRouting from '../../assets/infoboxRouting.svg'
import letsGo from '../../assets/letsGo.svg'
import saveIcon from '../../assets/saveIcon.svg'

function MobileInfobox() {
  const destination = useSelector(state => state.destination);
  const dispatch = useDispatch();
  const infobox = useSelector(state => state.infobox);

  return (
    <>
      <Container>
        <CloseButton onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "searchAndRouting" }); destination.remove(); }}>
          <img src={infoboxCloseButton} alt="infoboxCloseButton" />
        </CloseButton>

        <PlaceImage></PlaceImage>
        <InfoContainer>
          <NameParent>
            <img src={placeTypeIcon} alt="placeTypeIcon" />
            <PlaceName>{infobox.title}</PlaceName>
            <ShowMoreButton onClick={()=>{dispatch({ type: "UPDATE_USER_VIEW", payload: "infoboxFullScreen" })}}>
              بیشتر
              <img src={ShowMoreIcon} alt="ShowMoreIcon" />
            </ShowMoreButton>
          </NameParent>
          <TypeSection>
            <TypeText>{infobox.type}</TypeText>
            <img src={wheelChair} alt="wheelChair" />
            <Time>- <IsOpen>باز است</IsOpen> : ۷:۳۰ تا ۹</Time>
          </TypeSection>
          <Rating>
            ۳/۴
            <img src={mobileInfoboxStar} alt="mobileInfoboxStar" />
            ۸۳ رای
          </Rating>
          <div>
            <Location>
              <ImageWhitMargin src={locationIcon} alt="location" />
              {infobox.address}
            </Location>
            <Drive>
              <ImageWhitMargin src={carIcon} alt="carIcon" />
              ۱۲ دقیقه
              <SeparatorIconImg src={sepratorIcon} alt="separatorIcon" />
              ۸٫۳ کیلومتر
            </Drive>
          </div>
          <ButtonsContainer>
            <RoutingButton>
              <img src={infoboxRouting} alt="infoboxRouting" />
              مسیریابی
            </RoutingButton>
            <LightButton>
              <img src={letsGo} alt="letsGo" />
              بزن بریم
            </LightButton>
            <LightButton>
              <img src={saveIcon} alt="saveIcon" />
              ذخیره
            </LightButton>
          </ButtonsContainer>
        </InfoContainer>
      </Container>
    </>
  );
}

export default MobileInfobox