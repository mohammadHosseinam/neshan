import styled from '@emotion/styled';
import placeSourceImage from '../../assets/placeSourceImage.png';

export const Container = styled.div`
  position: absolute;
  bottom: 0;
  z-index: 1;
  width: 100%;
`;

export const CloseButton = styled.button`
  background-color: transparent;
  border: none;
  padding: 0px;
  margin: 0px;
`;

export const PlaceImage = styled.div`
  height: 91px;
  background-image: url(${placeSourceImage});
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 8px 8px 0px 0px;
`;

export const InfoContainer = styled.div`
  background-color: #fff;
  padding-top: 16px;
  padding-right: 8px;
  padding-left: 8px;
`;

export const NameParent = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;
`;

export const PlaceName = styled.h1`
  color: #000;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  flex-grow: 1;
`;

export const ShowMoreButton = styled.button`
  display: flex;
  align-items: end;
  gap: 6px;
  font-size: 12px;
  font-weight: 500;
  border-radius: 30px;
  background-color: rgba(25, 118, 210, 0.1);
  width: 75px;
  height: 24px;
  justify-content: center;
  color: #1976D2;
  font-family: Vazirmatn;
  border: none;
`;

export const TypeSection = styled.div`
  display: flex;
  align-items: center;
  margin-top: 10.5px;
`;

export const TypeText = styled.p`
  color: #000;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  margin-left: 7px;
  margin-right: 28px;
`;

export const Time = styled.p`
  margin-right: 8px;
  font-size: 12px;
  font-family: Vazirmatn;
`;

export const IsOpen = styled.span`
  color: #00B798;
`;

export const Rating = styled.div`
  color: #404040;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 11px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  display: flex;
  align-items: center;
  margin-right: 28px;
  margin-top: 14px;
  gap: 4px;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  gap: 8px;
  margin-top: 12px;
  padding-bottom: 14px;
`;

export const RoutingButton = styled.button`
  align-items: center;
  background-color: #1976D2;
  border-radius: 19.65px;
  border: none;
  color: #FFF;
  font-family: Vazirmatn;
  font-size: 16px;
  width: 113px;
  height: 37px;
  display: flex;
  justify-content: center;
`;

export const LightButton = styled.button`
  align-items: center;
  background-color: transparent;
  border-radius: 19.65px;
  border: 1.092px solid #E6E6E6;
  color: #000;
  font-family: Vazirmatn;
  font-size: 16px;
  height: 37px;
  display: flex;
  justify-content: center;
  padding: 0px 9px 0px 15px;
`;

export const Location = styled.div`
  margin-top: 14px;
  color: #000;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 13px;
  font-style: normal;
  font-weight: 400;
  display: flex;
  align-items: center;
`;

export const Drive = styled.div`
  margin-top: 6px;
  color: #000;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 13px;
  font-style: normal;
  font-weight: 400;
  display: flex;
  align-items: center;
`;

export const ImageWhitMargin = styled.img`
  margin-left: 8px;
`;

export const SeparatorIconImg = styled.img`
  margin-right: 9px;
  margin-left: 6px;
`;
