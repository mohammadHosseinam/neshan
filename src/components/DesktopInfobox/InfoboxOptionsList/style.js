import styled from '@emotion/styled';

export const ListContainer = styled.div`
  background: #fff;
  margin-bottom: 11px;
  padding-bottom: 47px;
`;

export const ListItemsBlack = styled.li`
  display: flex;
  align-items: center;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #000;
  gap: 8px;
`;

export const EditText = styled.li`
  display: flex;
  flex-direction: column;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #000;
`;

export const Flex = styled.div`
  display: flex;
`;

export const EditTextTitle = styled.p`
  color: #000;
  font-family: Vazirmatn;
  font-size: 14px;
  font-weight: 400;
  margin-right: 8px;
`;

export const EditTextSpan = styled.span`
  color: #a6a6a6;
  font-family: Vazirmatn;
  font-size: 12px;
  font-weight: 400;
  font-style: normal;
  margin-right: 32px;
`;

export const EditedBySpan = styled.span`
  color: #1976d2;
  font-size: 14px;
  font-weight: 400;
  font-family: Vazirmatn;
  margin-right: 10px;
`;

export const EditRating = styled.li`
  display: flex;
  flex-direction: column;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #000;
`;

export const EditRatingText = styled.p`
  color: #000;
  font-family: Vazirmatn;
  font-size: 14px;
  font-weight: 400;
  margin-right: 8px;
`;

export const EditRatingSpan = styled.span`
  color: #1976d2;
  font-family: Vazirmatn;
  font-size: 12px;
  font-weight: 400;
  font-style: normal;
  margin-right: 33px;
`;

export const Error = styled.li`
  width: 295px;
  height: 64px;
  background-color: rgba(235, 87, 87, 0.06);
  color: #eb5757;
  font-family: Vazirmatn;
  font-size: 12px;
  border-radius: 4px;
  display: flex;
  gap: 8px;
  padding-top: 6px;
  padding-right: 13px;
  margin-right: 21px;
`;

export const ErrorIcon = styled.img`
  margin-top: 7px;
  width: 12px;
  height: 12px;
`;
