import React from 'react'
import { EditRating, EditRatingSpan, EditRatingText, EditText, EditTextSpan, EditTextTitle, EditedBySpan, Error, ErrorIcon, Flex, ListContainer, ListItemsBlack } from './style'
import errorIcon from "../../../assets/errorIcon.svg"
import massageIcon from "../../../assets/massageIcon.svg"
import userIcon from "../../../assets/userIcon.svg"
import blueLocationIcon from "../../../assets/blueLocationIcon.svg"
import reportIcon from "../../../assets/reportIcon.svg"
import editIcon from "../../../assets/editIcon.svg"

function InfoboxOptionsList() {
    return (
      <div>
        <ListContainer>
          <ListItemsBlack>
            <img src={reportIcon} alt="reportIcon" />
            <span>گزارش اشکال نقشه</span>
          </ListItemsBlack>
          <EditText>
            <Flex>
              <img src={editIcon} alt="editIcon" />
              <EditTextTitle>ویرایش اطلاعات یا گزارش تعطیلی مکان</EditTextTitle>
            </Flex>
            <EditTextSpan>ویرایش شما در حال بررسی است</EditTextSpan>
          </EditText>
          <ListItemsBlack>
            <img src={blueLocationIcon} alt="blueLocationIcon" />
            <span>مختصات جغرافیایی</span>
          </ListItemsBlack>
          <ListItemsBlack>
            <img src={userIcon} alt="userIcon" />
            <span>آخرین ویرایش توسط:</span>
            <EditedBySpan>mostafa.javdani</EditedBySpan>
          </ListItemsBlack>
          <EditRating>
            <Flex>
              <img src={massageIcon} alt="massageIcon" />
              <EditRatingText>شما قبلا درباره این مکان نظر داده اید.</EditRatingText>
            </Flex>
            <EditRatingSpan>ویرایش نظر و امتیاز</EditRatingSpan>
          </EditRating>
          <Error>
            <ErrorIcon src={errorIcon} alt="errorIcon" />
            <span>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
            </span>
          </Error>
        </ListContainer>
      </div>
    );
  }
  
  export default InfoboxOptionsList;
  