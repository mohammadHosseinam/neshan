import styled from '@emotion/styled';

export const RatingContainer = styled.div`
  box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.12) inset;
`;

export const RatingTitle = styled.h4`
  color: #000;
  font-size: 16px;
  font-weight: 700;
  font-family: Vazirmatn;
  padding-top: 22px;
  margin-right: 76px;
  margin-bottom: 14px;
  width: 184px;
  text-align: center;
`;

export const RatingImage = styled.img`
  margin-right: 72.71px;
`;

export const InputContainer = styled.div`
  display: flex;
  margin-top: 13.72px;
  margin-right: 27.16px;
  padding-bottom: 21.82px;
`;

export const RatingInput = styled.input`
  width: 281.65878px;
  display: block;
  background-color: #fff;
  border: 1px solid #d9d9d9;
  border-radius: 8px;
  height: 33px;
  padding: 7.82px 11.84px 6.18px 62.82px;
  color: #a6a6a6;
  font-size: 12px;
  font-family: Vazirmatn;
`;