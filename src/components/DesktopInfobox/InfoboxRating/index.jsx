import React from 'react'
import { RatingImage, RatingInput, RatingTitle , RatingContainer, InputContainer } from './style'
import infoBoxRatingImage from "../../../assets/infoBoxRatingImage.svg"

function InfoboxRating() {
    return (
      <RatingContainer>
        <RatingTitle>به این مکان امتیاز دهید</RatingTitle>
        <RatingImage src={infoBoxRatingImage} alt="infoBoxRatingImage" />
        <InputContainer>
          <RatingInput type="text" placeholder='نظرتان را درباره این مکان بنویسید' />
        </InputContainer>
      </RatingContainer>
    );
  }
  
  export default InfoboxRating;