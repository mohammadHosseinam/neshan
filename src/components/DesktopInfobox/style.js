import styled from '@emotion/styled';

export const DesktopInfoboxContainer = styled.div `display: flex; position: absolute; z-index: 2;pointer-events: none`;

export const DesktopInfoboxWrapper = styled.div `pointer-events: all;background: #fafafa; height: 100vh; overflow-y: scroll; box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.12) inset; scrollbar-width: none; -ms-overflow-style: none; ::-webkit-scrollbar { display: none; }`;

export const ShowMoreButton = styled.button `width: 153px; height: 32px; border-radius: 16px; border: 1px solid rgba(25, 118, 210, 0.25); display: flex; align-items: center; justify-content: center; color: #1976D2; font-size: 14px; font-weight: 500; font-family: Vazirmatn; background: #FFF;`;

export const BackButton = styled.button `pointer-events: all;display: flex; align-items: center; justify-content: center; border: none; background-color: #fff; border-radius: 50%; width: 40px; height: 40px; margin-top: 16.39px; margin-right: 16px;`;

export const CommentSectionTitle = styled.h5 `color: #000; font-size: 16px; font-weight: 700; font-family: Vazirmatn; padding-top: 32px; padding-right: 34px; padding-bottom: 21px;`;

export const WhiteBackground = styled.div` background: #fff;`;

export const ChartImage = styled.img `margin: 0px 6px 14px;`;

export const ChartImageContainer = styled.div `background-color: #fff; width: 100%;`;

export const ShowMoreButtonContainer = styled.div`margin-right: 81px; margin-top: 21px; padding-bottom: 21px;`;

export const TrafficInfo = styled.div `font-family: Vazirmatn; font-size: 14px; font-weight: 400; color: #000; margin-top: 12px; margin-right: 6px; margin-bottom: 10px; display: flex; gap: 8px;`;