import styled from '@emotion/styled';

export const ListItemBlack = styled.li`
  display: flex;
  align-items: center;
  gap: 8px;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #000;
`;

export const AddNumberForPlace = styled.li`
  display: flex;
  gap: 8px;
  align-items: center;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #1976D2;
`;

export const WhenIsOpen = styled.li`
  display: flex;
  flex-direction: column;
  margin-right: 21px;
  padding: 12px 0;
  width: 292px;
  border-bottom: 1px solid #f2f2f2;
  font-size: 14px;
  font-family: vazirmatn;
  font-weight: 400;
  color: #000;
`;

export const OpenLink = styled.img`
  margin: 0px 10px 0px 5px;
`;

export const Flex = styled.div`
  display: flex;
`;

export const OpenTime = styled.p`
  color: #000;
  font-family: Vazirmatn;
  font-size: 14px;
  font-weight: 400;
  margin-right: 8px;
`;

export const IsOpenSpan = styled.span`
  color: #5CD88E;
  font-family: Vazirmatn;
  font-size: 14px;
  font-weight: 700;
  margin-right: 20px;
`;

export const CoronaWarning = styled.div`
  color: #FFAE34;
  font-family: Vazirmatn;
  font-size: 12px;
  font-weight: 400;
  font-style: normal;
  margin-right: 30px;
`;
