import React from 'react'
import { AddNumberForPlace, CoronaWarning, Flex, IsOpenSpan, ListItemBlack, OpenLink, OpenTime, WhenIsOpen } from './style'
import { useSelector } from 'react-redux'
import infoboxLocationIcon from "../../../assets/infoboxLocationIcon.svg"
import infoboxPhoneIcon from "../../../assets/infoboxPhoneIcon.svg"
import infoboxAddPhoneIcon from "../../../assets/infoboxAddPhoneIcon.svg"
import infoboxWebsiteIcon from "../../../assets/infoboxWebsiteIcon.svg"
import infoboxOpenLinkIcon from "../../../assets/infoboxOpenLinkIcon.svg"
import infoboxClockIcon from "../../../assets/infoboxClockIcon.svg"


function InfoboxInformationsList() {
    const infobox = useSelector(state => state.infobox)
    return (
        <ul>
          <ListItemBlack>
            <img src={infoboxLocationIcon} alt="infoboxLocationIcon" />
            <span>{infobox.address}</span>
          </ListItemBlack>
          <ListItemBlack>
            <img src={infoboxPhoneIcon} alt="infoboxPhoneIcon" />
            <span>
              تلفن تماس : 021 - 8437846
            </span>
          </ListItemBlack>
          <AddNumberForPlace>
            <img src={infoboxAddPhoneIcon} alt="infoboxAddPhoneIcon" />
            <span>افزودن شماره تماس برای این مکان</span>
          </AddNumberForPlace>
          <ListItemBlack>
            <img src={infoboxWebsiteIcon} alt="infoboxWebsiteIcon" />
            <span>آدرس سایت</span>
            <OpenLink src={infoboxOpenLinkIcon} alt="infoboxOpenLinkIcon" />
            <span>www.mobinet.com</span>
          </ListItemBlack>
          <WhenIsOpen>
            <Flex>
              <img src={infoboxClockIcon} alt="infoboxClockIcon" />
              <OpenTime>ساعت کاری: 9 تا 17</OpenTime>
              <IsOpenSpan>باز است</IsOpenSpan>
            </Flex>
            <CoronaWarning>شیوع کرونا ممکن است در ساعت کاری تاثیر داشته باشد</CoronaWarning>
          </WhenIsOpen>
        </ul>
      );
      
}

export default InfoboxInformationsList