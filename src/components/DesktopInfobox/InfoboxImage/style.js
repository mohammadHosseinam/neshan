import styled from '@emotion/styled';
import InfoboxImage from "../../../assets/InfoboxImage.png";

export const InfoboxImageContainer = styled.div`
  background: url(${InfoboxImage}), lightgray 50% / cover no-repeat;
  width: 340px;
  height: 256px;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
  padding: 16px 25px;
`;
export const AddImageButton = styled.button`
  border-radius: 68px;
  border: 1px solid #FFF;
  background: rgba(0, 0, 0, 0.50);
  color: #FFF;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 15px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  display: flex;
  align-items: center;
  width: 126px;
  height: 31px;
`;