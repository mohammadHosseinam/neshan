import React from 'react'
import { AddImageButton, InfoboxImageContainer } from './style'
import addImageIcon from "../../../assets/addImageIcon.svg"
const InfoboxImage = () => {
    return (
      <InfoboxImageContainer>
        <AddImageButton>
          <img src={addImageIcon} alt="addImageIcon" />
          <span>افزودن عکس</span>
        </AddImageButton>
      </InfoboxImageContainer>
    );
  };
  
  export default InfoboxImage;