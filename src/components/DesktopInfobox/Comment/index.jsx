import React from 'react'
import { CommentDate, CommentText, Container, Flex, FlexGrow, Profile, UserInfo, UserName } from './style'
import infoboxCommentRating from "../../../assets/infoboxCommentRating.svg"


const Comment = () => {
  return (
    <Container>
      <Flex>
        <Profile/>
        <UserInfo>
          <UserName>علی رضاییان مقدم</UserName>
          <Flex>
            <img src={infoboxCommentRating} alt="infoboxCommentRating" />
            <FlexGrow />
            <CommentDate>12اردیبهشت 1399</CommentDate>
          </Flex>
        </UserInfo>
      </Flex>
      <CommentText>بهترین رستورانی که تو این محل می شناسم.</CommentText>
    </Container>
  );
};

export default Comment;