import userAvatar from "../../../assets/userAvatar.png";
import styled from '@emotion/styled';

export const Container = styled.div`
  margin: 13px 24px 0px;
  padding-bottom: 13px;
  border-bottom: 1px solid #f2f2f2;
`;

export const  Flex = styled.div`
  display: flex;
`;

export const  Profile = styled.div`
  background: url(${userAvatar}), lightgray 50% / cover no-repeat;
  border-radius: 16px;
  width: 32px;
  height: 32px;
`;

export const  UserInfo = styled.div`
  margin-right: 10px;
  flex-grow: 1;
`;

export const  UserName = styled.h5`
  font-size: 12px;
  font-weight: 500;
  font-family: Vazirmatn;
  color: #808080;
  margin-bottom: 8px;
`;

export const  FlexGrow = styled.div`
  flex-grow: 1;
`;

export const  CommentDate = styled.span`
  font-size: 10px;
  font-family: Vazirmatn;
  color: #BFBFBF;
`;

export const  CommentText = styled.p`
  margin-right: 42px;
  font-size: 12px;
  font-family: Vazirmatn;
  color: #404040;
  margin-top: 7px;
`;