import React from 'react'
import { AddButton, ButtonText, InfoboxButtonsContainer, InfoboxButtonsText, RoutingButton } from './style'
import infoboxRoutingButtonIcon from "../../../assets/infoboxRoutingButtonIcon.svg"
import infoboxSavingButtonIcon from "../../../assets/infoboxSavingButtonIcon.svg"
import { useDispatch, useSelector } from 'react-redux';

const InfoboxButtons = () => {
  const infobox = useSelector(state => state.infobox)
  const dispatch = useDispatch()
  return (
    <InfoboxButtonsContainer>
      <InfoboxButtonsText>
        <div>
          <RoutingButton onClick={()=>{dispatch({ type: "UPDATE_USER_VIEW", payload: "routing" });dispatch({type: "UPDATE_DESTINATION_INPUT_VALUE",payload:infobox.title})}}>
            <img src={infoboxRoutingButtonIcon} alt="infoboxRoutingButtonIcon" />
          </RoutingButton>
        </div>
        <p>مسیریابی</p>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={infoboxSavingButtonIcon} alt="infoboxSavingButtonIcon" />
          </AddButton>
        </div>
        <ButtonText>معابر ترافیک</ButtonText>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={infoboxSavingButtonIcon} alt="infoboxSavingButtonIcon" />
          </AddButton>
        </div>
        <ButtonText>دوربین ترافیکی</ButtonText>
      </InfoboxButtonsText>
      <InfoboxButtonsText>
        <div>
          <AddButton>
            <img src={infoboxSavingButtonIcon} alt="infoboxSavingButtonIcon" />
          </AddButton>
        </div>
        <ButtonText>رویداد ها</ButtonText>
      </InfoboxButtonsText>
    </InfoboxButtonsContainer>
  );
};

export default InfoboxButtons;