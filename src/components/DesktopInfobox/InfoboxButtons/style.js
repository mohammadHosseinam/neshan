import styled from '@emotion/styled';

export const InfoboxButtonsContainer = styled.div`
  display: flex;
  align-items: center;
  margin-top: 22px;
  padding-right: 34px;
  padding-left: 27px;
  justify-content: space-between;
  border-bottom: 1px solid #F2F2F2;
  padding-bottom: 21px;
`;

export const InfoboxButtonsText = styled.div`
  font-weight: 700;
  font-size: 12px;
  font-family: Vazirmatn;
  color: #404040;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const AddButton = styled.button`
  border: 1.5px solid #1976D2;
  border-radius: 14px;
  padding: 9.13px;
  margin-bottom: 8px;
  background-color: #fafafa;
`;

export const ButtonText = styled.p`
  font-weight: 500;
`;

export const RoutingButton = styled.button`
  border-radius: 14px;
  background: #1976D2;
  padding: 9.13px;
  border: none;
  margin-bottom: 7.5px;
`;