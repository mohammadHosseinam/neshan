import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { BackButton, ChartImage, ChartImageContainer, CommentSectionTitle, DesktopInfoboxContainer, DesktopInfoboxWrapper, ShowMoreButton, ShowMoreButtonContainer, TrafficInfo, WhiteBackground } from './style';
import InfoboxChart from "../../assets/InfoboxChart.png"
import infoboxBackIcon from "../../assets/infoboxBackIcon.svg"
import traficHoursIcon from "../../assets/traficHoursIcon.svg"
import InfoboxImage from './InfoboxImage';
import InfoboxIntro from './InfoboxIntro';
import InfoboxButtons from './InfoboxButtons';
import InfoboxInformationsList from './InfoboxInformationsList';
import InfoboxRating from './InfoboxRating';
import InfoboxOptionsList from './InfoboxOptionsList';
import Comment from './Comment';

function DesktopInfobox() {
  const destination = useSelector(state => state.destination);
  const dispatch = useDispatch();
  return (
    <DesktopInfoboxContainer>
      <DesktopInfoboxWrapper>
        <InfoboxImage />
        <InfoboxIntro />
        <InfoboxButtons />
        <InfoboxInformationsList />
        <TrafficInfo> <img src={traficHoursIcon} alt="traficHoursIcon" /> <span>ساعت های پرتردد</span> </TrafficInfo>
        <ChartImageContainer>
          <ChartImage src={InfoboxChart} alt="chart of views here" />
        </ChartImageContainer>
        <InfoboxRating />
        <InfoboxOptionsList />
        <WhiteBackground>
          <CommentSectionTitle>نظرات کاربران درباره این مکان</CommentSectionTitle>
          <div>
            <Comment />
            <Comment />
            <Comment />
          </div>
          <ShowMoreButtonContainer>
            <ShowMoreButton>سایر نظرات</ShowMoreButton>
          </ShowMoreButtonContainer> 
          </WhiteBackground>
      </DesktopInfoboxWrapper>
      <div>
        <BackButton onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "searchAndRouting" }); destination.remove(); destination._lngLat = { lng: "", lat: "" }; console.log(destination) }}> <img src={infoboxBackIcon} alt="infoboxBackIcon" />
        </BackButton>
      </div>
    </DesktopInfoboxContainer>);
}

export default DesktopInfobox;