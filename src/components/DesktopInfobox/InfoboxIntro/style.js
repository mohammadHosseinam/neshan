import styled from '@emotion/styled';

export const InfoboxIntroContainer = styled.div`
  background-color: #fff;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.08);
  height: 130px;
`;

export const InfoboxTitle = styled.div`
  font-family: Vazirmatn;
  color: #000;
  font-size: 18px;
  font-weight: 700;
  display: flex;
  align-items: center;
  gap: 10px;
  padding-top: 14px;
  margin-right: 19px;
`;

export const InfoboxPlaceType = styled.p`
  color: #808080;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 14px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  margin-right: 50px;
  margin-bottom: 6px;
`;

export const InfoboxRate = styled.div`
  color: #808080;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 11px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
  display: flex;
  align-items: center;
  margin-right: 53px;
  gap: 10px;
`;

export const InfoboxTimeToArive = styled.div`
  display: flex;
  align-items: center;
  margin-right: 19px;
  margin-top: 10px;
`;

export const MinutTextStyles = styled.span`
  color: #808080;
  text-align: right;
  font-family: Vazirmatn;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: normal;
`;

export const MinutIntegerStyles = styled.p`
  color: #000;
  font-family: Vazirmatn;
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  margin-left: 4px;
`;

export const InfoboxRateImage = styled.img`
  margin-right: 5px;
`;

export const InfoboxIntroCarIcon = styled.img`
  margin-left: 8px;
`;

export const SperatorLine = styled.img`
  margin: 0 12px;
`;