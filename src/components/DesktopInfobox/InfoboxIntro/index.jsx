import React from 'react'
import { useSelector } from 'react-redux'
import { InfoboxIntroCarIcon, InfoboxIntroContainer, InfoboxPlaceType, InfoboxRate, InfoboxRateImage, InfoboxTimeToArive, InfoboxTitle, MinutIntegerStyles, MinutTextStyles, SperatorLine } from './style'
import infoboxIntroPlaceIcon from "../../../assets/infoboxIntroPlaceIcon.svg"
import infoboxIntroStarsIcon from "../../../assets/infoboxIntroStarsIcon.svg"
import infoboxIntroCarIcon from "../../../assets/infoboxIntroCarIcon.svg"
import infoboxIntroSeperatorLine from "../../../assets/infoboxIntroSeperatorLine.svg"

function InfoboxIntro() {
  const infobox = useSelector(state => state.infobox);
  return (
    <InfoboxIntroContainer>
      <InfoboxTitle>
        <img src={infoboxIntroPlaceIcon} alt="infoboxIntroPlaceIcon" />
        {infobox.title}
      </InfoboxTitle>
      <InfoboxPlaceType>{infobox.type}</InfoboxPlaceType>
      <InfoboxRate>
        <span>۳/۴</span>
        <InfoboxRateImage src={infoboxIntroStarsIcon} alt="infoboxIntroStarsIcon" />
        <span>۸۳ رای</span>
      </InfoboxRate>
      <InfoboxTimeToArive>
        <InfoboxIntroCarIcon src={infoboxIntroCarIcon} alt="infoboxIntroCarIcon" />
        <MinutIntegerStyles>۱۲</MinutIntegerStyles>
        <MinutTextStyles>دقیقه</MinutTextStyles>
        <SperatorLine src={infoboxIntroSeperatorLine} alt="infoboxIntroSeperatorLine" />
        <MinutIntegerStyles>۴۵</MinutIntegerStyles>
        <MinutTextStyles>دقیقه</MinutTextStyles>
      </InfoboxTimeToArive>
    </InfoboxIntroContainer>
  );
}

export default InfoboxIntro;
