import styled from '@emotion/styled';

export const InputBox = styled.div`
  box-shadow: 0 4px 12px rgba(0, 0, 0, .25);
  position: absolute;
  z-index: 1;
  height: 40px;
  right: 27px;
  top: 27px;
  width: 300px;
  background-color: #f5f5f5;
  align-items: center;
  border: 1px solid #d9d9d9;
  border-radius: 14px;
  display: flex;
  height: 40px;
  justify-content: space-between;
  padding: 8px;
  pointer-events: auto;
  transition: .5s;
`;

export const RouttinButton = styled.button`
  border-radius: 8px;
  background: #1976D2;
  padding: 4px;
  padding-bottom: 0px;
  border: 1px solid #1976D2;
`;

export const SearchInput = styled.div`
  background-color: #f5f5f5;
  border: none;
  flex-grow: 1;
  margin-right: 8px;
  font-size: 14px;
`;

export const SepratorLine = styled.span`
  width: 1px;
  height: 24px;
  margin: 0 5px;
  background-color: #d9d9d9;
`;