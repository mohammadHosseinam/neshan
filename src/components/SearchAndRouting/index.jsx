import React from 'react';
import { useDispatch } from 'react-redux';
import { InputBox, RouttinButton, SearchInput, SepratorLine } from './style.js';
import menuIcon from '../../assets/menuIcon.svg';
import routingIcon from '../../assets/routingIcon.svg';
import searchIcon from '../../assets/searchIcon.svg';

function SearchAndRouting() {
  const dispatch = useDispatch();
  return (
    <div>
      <InputBox>
        <img src={menuIcon} alt="menuIcon" />
        <SearchInput onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "search" }) }}>جستجو</SearchInput>
        <img src={searchIcon} alt="searchIcon" onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "search" }) }} />
        <SepratorLine />
        <RouttinButton onClick={() => { dispatch({ type: "UPDATE_USER_VIEW", payload: "routing" }) }}>
          <img src={routingIcon} alt="routingIcon" />
        </RouttinButton>
      </InputBox>
    </div>
  );
}

export default SearchAndRouting;
