import './App.css'
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl'
import { useRef, useEffect } from 'react';
import { css } from '@emotion/css';
import SearchAndRouting from './components/searchAndRouting';
import DesktopLeftSideButton from './components/DesktopLeftSideButton';
import DesktopInfobox from './components/DesktopInfobox';
import useDeviceType from './hooks/useDeviceType';
import MobileSearchAndRouting from './components/MobileSearchAndRouting';
import MobileSearch from './components/MobileSearch';
import MobileInfobox from './components/MobileInfobox';
import MobileInfoboxFullScreen from './components/MobileInfoboxFullScreen';
import { useDispatch, useSelector } from 'react-redux';
import Desktop from './clients/desktop';
import Mobile from './clients/mobile';

mapboxgl.setRTLTextPlugin(
  'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.3/mapbox-gl-rtl-text.js',
  null,
  true // Lazy load the plugin
);
function App() {
  mapboxgl.accessToken = 'pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw';
  const map = useSelector(state => state.map)
  const origin = useSelector(state => state.origin)
  const destination = useSelector(state => state.destination)
  const selectingOrigin = useSelector(state => state.selectingOrigin)
  const userView = useSelector(state => state.userView)
  const dispatch = useDispatch()
  useEffect(() => {
    map.on("click", handelClickFunction)
    return (() => { map.off("click", handelClickFunction) })
  }, [selectingOrigin, userView])

  const handelClickFunction = (e) => {
    if (userView === "routing") {
      console.log(userView)
      if (selectingOrigin) {
        fetch(`https://api.neshan.org/v5/reverse?lat=${e.lngLat.lat}&lng=${e.lngLat.lng}`, {
          method: "GET",
          headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
        })
          .then((resp) => resp.json())
          .then((data) => {
            const title = data.place || data.route_name || data.formatted_address;
            dispatch({ type: "UPDATE_ORIGIN_INPUT_VALUE", payload: title })
          })
        origin.setLngLat([e.lngLat.lng, e.lngLat.lat]).addTo(map)
        dispatch({ type: "UPDATE_ORIGIN_SUGGESTIONS", payload: [] })
        dispatch({ type: 'UPDATE_ORIGIN_SELECTING', payload: true })
      } else {
        console.log(userView)
        fetch(`https://api.neshan.org/v5/reverse?lat=${e.lngLat.lat}&lng=${e.lngLat.lng}`, {
          method: "GET",
          headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
        })
          .then((resp) => resp.json())
          .then((data) => {
            const title = data.place || data.route_name || data.formatted_address;
            dispatch({ type: "UPDATE_DESTINATION_INPUT_VALUE", payload: title })
          })
        destination.setLngLat([e.lngLat.lng, e.lngLat.lat]).addTo(map)
        dispatch({ type: 'UPDATE_ORIGIN_SELECTING', payload: false })
        dispatch({ type: "UPDATE_ORIGIN_SUGGESTIONS", payload: [] })
      }
    } else {
      console.log(destination)
      destination.setLngLat([e.lngLat.lng, e.lngLat.lat]).addTo(map)
      fetch(`https://api.neshan.org/v5/reverse?lat=${e.lngLat.lat}&lng=${e.lngLat.lng}`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then((resp) => resp.json())
        .then((data) => { console.log(data); const title = data.place || data.route_name; dispatch({ type: "UPDATE_INFOBOX", payload: { address: data.formatted_address, type: data.neighbourhood, title, region: data.county, location: {} } }); dispatch({ type: "UPDATE_USER_VIEW", payload: "infobox" }) }); map.flyTo({
          center: [e.lngLat.lng, e.lngLat.lat],
          essential: true
        })
    }
  }
  const HandelDragFunction = (e) => {
    console.log(e.target._lngLat)
    const lng = e.target._lngLat.lng
    const lat = e.target._lngLat.lat

    destination.setLngLat([lng, lat]).addTo(map)
    if (userView === "routing") {
      console.log(userView)
      fetch(`https://api.neshan.org/v5/reverse?lat=${lat}&lng=${lng}`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then((resp) => resp.json())
        .then((data) => { console.log(data); const title = data.place || data.route_name; dispatch({ type: "UPDATE_DESTINATION_INPUT_VALUE", payload: title }); dispatch({ type: "UPDATE_DESTINATION_SUGGESTIONS", payload: [] }); })
    } else {
      fetch(`https://api.neshan.org/v5/reverse?lat=${lat}&lng=${lng}`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then((resp) => resp.json())
        .then((data) => { const title = data.place || data.route_name; dispatch({ type: "UPDATE_INFOBOX", payload: { address: data.formatted_address, type: data.neighbourhood, title, region: data.county, location: {} } }); dispatch({ type: "UPDATE_USER_VIEW", payload: "infobox" }) })
    }
  }
  useEffect(() => {
    destination.on("dragend", HandelDragFunction)
    return (() => { destination.off("dragend", HandelDragFunction) })
  }, [userView])
  origin.on("dragend", (e)=>{
    const lng = e.target._lngLat.lng
    const lat = e.target._lngLat.lat
    fetch(`https://api.neshan.org/v5/reverse?lat=${lat}&lng=${lng}`, {
        method: "GET",
        headers: { "Api-Key": "service.8c48513ba2b341c4b8c1394825c8e392" },
      })
        .then((resp) => resp.json())
        .then((data) => { console.log(data); const title = data.place || data.route_name; dispatch({ type: "UPDATE_ORIGIN_INPUT_VALUE", payload: title }); dispatch({ type: "UPDATE_DESTINATION_SUGGESTIONS", payload: [] }); })
  })
  const isMobile = useDeviceType()
  return (
    <>
      {isMobile ? <Mobile /> : <Desktop />}
    </>
  )
}

export default App
