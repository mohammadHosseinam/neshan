import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import 'mapbox-gl/dist/mapbox-gl.css';
import mapboxgl from 'mapbox-gl'
import { Provider } from 'react-redux';
import store from './redux/store.js';

// map.current = new mapboxgl.Map({
//   container: "map",
//   style: 'mapbox://styles/mapbox/streets-v12',
//   center: [51.465831, 35.690494],
//   zoom: 12
// });
//map.current
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
    <App />
    </Provider>
  </React.StrictMode>,
)

 