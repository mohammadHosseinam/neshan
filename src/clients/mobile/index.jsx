import React from 'react'
import { useSelector } from 'react-redux';
import MobileSearchAndRouting from '../../components/MobileSearchAndRouting';
import MobileInfobox from '../../components/MobileInfobox';
import MobileSearch from '../../components/MobileSearch';
import MobileInfoboxFullScreen from '../../components/MobileInfoboxFullScreen';

function Mobile() {
  const userView = useSelector(state => state.userView)
  return (
    <>
    {userView === "searchAndRouting" && <MobileSearchAndRouting/>}
    {userView === "infobox" && <MobileInfobox/>} 
    { userView === "search" && <MobileSearch/>}
    { userView === "infoboxFullScreen" && <MobileInfoboxFullScreen/>}
    </>
  )
}

export default Mobile