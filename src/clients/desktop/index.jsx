import React from 'react'
import { useSelector } from 'react-redux';
import SearchAndRouting from '../../components/searchAndRouting';
import DesktopInfobox from '../../components/DesktopInfobox';
import DesktopLeftSideButton from '../../components/DesktopLeftSideButton';
import DesktopSearch from '../../components/DesktopSearch';
import DesktopRouting from '../../components/DesktopRouting';

function Desktop() {
  const userView = useSelector(state => state.userView)
  return (
    <>
      <DesktopLeftSideButton />
      {userView === "searchAndRouting" && <SearchAndRouting />}
      {userView === "infobox" && <DesktopInfobox />}
      {userView === "search" && <DesktopSearch />}
      {userView === "routing" && <DesktopRouting />}
      
    </>
  )
}

export default Desktop